#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui/constraintscontroller.h"
#include "ui/mazegraphicsscene.h"
#include "ui/solutioncontroller.h"
#include "ui/solvercontroller.h"

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:

    void resizeEvent(QResizeEvent * event) override;

private:

    void setupToolButtons();

    void setupConnections();

    Ui::MainWindow *ui;
    MazeGraphicsScene * _scene;
    ConstraintsController * _constraintsController;
    SolutionController * _solutionController;
    SolverController * _solverController;
    QGraphicsView * _view;
    QTimer _timer;

private slots:

    void onStartClicked();

    void onSolveClicked();

    void onResizeTimerTimout();

    void onToolButtonTriggered(QAction * action);
};
#endif // MAINWINDOW_H
