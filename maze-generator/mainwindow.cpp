#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

namespace {
    const int ICON_SIZE = 64;

    const std::vector<std::pair<QString, ConnectionGroup>> toolButtons{
        std::pair<QString,ConnectionGroup>{"toolButton_1", ConnectionGroup::Maroon},
        std::pair<QString,ConnectionGroup>{"toolButton_2", ConnectionGroup::Red},
        std::pair<QString,ConnectionGroup>{"toolButton_4", ConnectionGroup::Pink},
        std::pair<QString,ConnectionGroup>{"toolButton_3", ConnectionGroup::Lavendar},
        std::pair<QString,ConnectionGroup>{"toolButton_5", ConnectionGroup::Magenta},
        std::pair<QString,ConnectionGroup>{"toolButton_6", ConnectionGroup::Purple},
        std::pair<QString,ConnectionGroup>{"toolButton_8", ConnectionGroup::Blue},
        std::pair<QString,ConnectionGroup>{"toolButton_7", ConnectionGroup::Teal},
        std::pair<QString,ConnectionGroup>{"toolButton_9", ConnectionGroup::Cyan},
        std::pair<QString,ConnectionGroup>{"toolButton_10", ConnectionGroup::Mint},
        std::pair<QString,ConnectionGroup>{"toolButton_12", ConnectionGroup::Lime},
        std::pair<QString,ConnectionGroup>{"toolButton_11", ConnectionGroup::Green},
        std::pair<QString,ConnectionGroup>{"toolButton_13", ConnectionGroup::Olive},
        std::pair<QString,ConnectionGroup>{"toolButton_14", ConnectionGroup::Yellow},
        std::pair<QString,ConnectionGroup>{"toolButton_16", ConnectionGroup::Beige},
        std::pair<QString,ConnectionGroup>{"toolButton_15", ConnectionGroup::Coral},
        std::pair<QString,ConnectionGroup>{"toolButton_17", ConnectionGroup::Orange},
        std::pair<QString,ConnectionGroup>{"toolButton_18", ConnectionGroup::Brown}
    };
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      _scene(new MazeGraphicsScene(this)),
      _constraintsController(new ConstraintsController(_scene)),
      _solutionController(new SolutionController(_scene)),
      _solverController(new SolverController(this))
{
    _timer.setInterval(100);
    _timer.setSingleShot(true);

    ui->setupUi(this);
    setupToolButtons();
    setupConnections();

    _view = this->findChild<QGraphicsView*>("graphicsView");
    _view->setScene(_scene);
}

MainWindow::~MainWindow()
{
    delete _solverController;
    delete _solutionController;
    delete _constraintsController;
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);

    _timer.start();
}

void MainWindow::setupToolButtons()
{
    for(auto pair : toolButtons) {
        auto button = this->findChild<QToolButton*>(pair.first);
        QPixmap pixmap(ICON_SIZE,ICON_SIZE);
        pixmap.fill(getColor(pair.second));
        QIcon icon(pixmap);
        QAction * action = new QAction(icon, getString(pair.second), this);
        action->setData(QVariant::fromValue(pair.second));

        button->setDefaultAction(action);
        button->setFixedSize(ICON_SIZE, ICON_SIZE);
        button->setIconSize(QSize(ICON_SIZE-8, ICON_SIZE-8));

        connect(button, &QToolButton::triggered, this, &MainWindow::onToolButtonTriggered);
    }
}

void MainWindow::setupConnections()
{
    connect(&_timer, &QTimer::timeout, this, &MainWindow::onResizeTimerTimout);

    connect(this->findChild<QPushButton*>("start_Button"), &QPushButton::clicked,
            this, &MainWindow::onStartClicked);
    connect(this->findChild<QPushButton*>("clearGroup_Button"), &QPushButton::clicked,
            _scene, &MazeGraphicsScene::onClearSelected);

    connect(this->findChild<QSpinBox*>("mazeWidth_SpinBox"), QOverload<int>::of(&QSpinBox::valueChanged),
            _constraintsController, &ConstraintsController::onWidthChanged);
    connect(this->findChild<QSpinBox*>("mazeDepth_SpinBox"), QOverload<int>::of(&QSpinBox::valueChanged),
            _constraintsController, &ConstraintsController::onDepthChanged);
    connect(this->findChild<QSpinBox*>("mazeHeight_SpinBox"), QOverload<int>::of(&QSpinBox::valueChanged),
            _constraintsController, &ConstraintsController::onHeightChanged);
    connect(this->findChild<QPushButton*>("start_Button"), &QPushButton::clicked,
            _constraintsController, &ConstraintsController::onStart);
    connect(this->findChild<QPushButton*>("solve_Button"), &QPushButton::clicked,
            _constraintsController, &ConstraintsController::onSolve);
    connect(_scene, &MazeGraphicsScene::constraintChanged,
            _constraintsController, &ConstraintsController::onConstraintChanged);
    connect(_scene, &MazeGraphicsScene::groupsChanged,
            _constraintsController, &ConstraintsController::onGroupsChanged);

    connect(this->findChild<QPushButton*>("solve_Button"), &QPushButton::clicked,
            this, &MainWindow::onSolveClicked);

    connect(this->findChild<QSpinBox*>("mazeWidth_SpinBox"), QOverload<int>::of(&QSpinBox::valueChanged),
            _solverController, &SolverController::onWidthChanged);
    connect(this->findChild<QSpinBox*>("mazeDepth_SpinBox"), QOverload<int>::of(&QSpinBox::valueChanged),
            _solverController, &SolverController::onDepthChanged);
    connect(this->findChild<QSpinBox*>("mazeHeight_SpinBox"), QOverload<int>::of(&QSpinBox::valueChanged),
            _solverController, &SolverController::onHeightChanged);
    connect(_constraintsController, &ConstraintsController::constraintsChanged,
            _solverController, &SolverController::onConstraintsChanged);
    connect(this->findChild<QPushButton*>("solve_Button"), &QPushButton::clicked,
            _solverController, &SolverController::onSolve);

    connect(this->findChild<QPushButton*>("reset_Button"), &QPushButton::clicked,
            _solutionController, &SolutionController::onReset);
    connect(this->findChild<QPushButton*>("step_Button"), &QPushButton::clicked,
            _solutionController, &SolutionController::onStep);
    connect(this->findChild<QPushButton*>("watch_Button"), &QPushButton::clicked,
            _solutionController, &SolutionController::onWatch);
    connect(this->findChild<QPushButton*>("run_Button"), &QPushButton::clicked,
            _solutionController, &SolutionController::onRun);
    connect(this->findChild<QPushButton*>("result_Button"), &QPushButton::clicked,
            _solutionController, &SolutionController::onResult);
    connect(_solverController, &SolverController::solutionReady,
            _solutionController, &SolutionController::displaySolution);
}

void MainWindow::onStartClicked()
{
    _solutionController->setActive(false);
    _constraintsController->setActive(true);

    this->findChild<QPushButton*>("clearGroup_Button")->setEnabled(true);
    for(auto button : this->findChildren<QToolButton*>()) {
        button->setEnabled(true);
    }
}

void MainWindow::onSolveClicked()
{
    _constraintsController->setActive(false);
    _solutionController->setActive(true);

    this->findChild<QPushButton*>("clearGroup_Button")->setEnabled(false);
    for(auto button : this->findChildren<QToolButton*>()) {
        button->setEnabled(false);
    }
}

void MainWindow::onResizeTimerTimout()
{
    _view->fitInView(_scene->sceneRect(), Qt::KeepAspectRatio);
    _scene->update();
}

void MainWindow::onToolButtonTriggered(QAction *action)
{
    auto group = action->data().value<ConnectionGroup>();
    _scene->setGroupSelector(group);
}
