QT       += core gui

SOURCES += \
    $$PWD/lib/basetypes.cpp \
    $$PWD/lib/solutionstepper.cpp \
    $$PWD/ui/constraintroomitem.cpp \
    $$PWD/ui/constraintscontroller.cpp \
    $$PWD/ui/constrainttopitem.cpp \
    $$PWD/ui/mazegraphicsscene.cpp \
    $$PWD/ui/mazeroomitem.cpp \
    $$PWD/ui/mazetopitem.cpp \
    $$PWD/ui/solutioncontroller.cpp \
    $$PWD/ui/solvercontroller.cpp \
    $$PWD/ui/ui_types.cpp \
    lib/constraints.cpp \
    lib/mazebuilder.cpp \
    lib/maze.cpp \
    lib/solver.cpp

HEADERS += \
    $$PWD/lib/basetypes.h \
    $$PWD/lib/optional.h \
    $$PWD/lib/solutionstepper.h \
    $$PWD/ui/constraintroomitem.h \
    $$PWD/ui/constraintscontroller.h \
    $$PWD/ui/constrainttopitem.h \
    $$PWD/ui/mazegraphicsscene.h \
    $$PWD/ui/mazeroomitem.h \
    $$PWD/ui/mazetopitem.h \
    $$PWD/ui/solutioncontroller.h \
    $$PWD/ui/solvercontroller.h \
    $$PWD/ui/ui_types.h \
    lib/constraints.h \
    lib/mazebuilder.h \
    lib/maze.h \
    lib/solver.h \
    lib/walls.h
