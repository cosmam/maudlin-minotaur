#ifndef SOLUTIONCONTROLLER_H
#define SOLUTIONCONTROLLER_H

#include "lib/solutionstepper.h"
#include "lib/solver.h"
#include "mazeroomitem.h"
#include "mazetopitem.h"

#include <QGraphicsScene>
#include <QObject>
#include <QPointer>
#include <QTimer>

class SolutionController : public QObject
{
    Q_OBJECT

public:
    SolutionController(QGraphicsScene * scene);

    void setActive(bool isActive);

    bool isActive() const { return _isActive; }

public slots:

    void onMazeTypeChanged(QString mazeTypeString);

    void displaySolution(Solver::Solution solution);

    void onReset();

    void onStep();

    void onWatch();

    void onRun();

    void onResult();

private:

    void clearDisplay();

    void displayMaze();

    QPointer<QGraphicsScene> _scene;
    QPointer<QTimer> _timer;
    SolutionStepper _stepper;
    tl::optional<SteppingState> _stepperGoal;
    tl::optional<Solver::Solution> _solution;
    std::map<Position, MazeRoomItem *> _rooms;
    std::map<Position, MazeTopItem *> _tops;
    bool _isActive;
    MazeType _mazeType;

private slots:

    void step();

};

#endif // SOLUTIONCONTROLLER_H
