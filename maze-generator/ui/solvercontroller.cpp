#include "solvercontroller.h"

#include "lib/constraints.h"
#include "lib/maze.h"

SolverController::SolverController(QObject *parent)
    : QObject(parent), _size{1,1,1}, _constraints(_size)
{

}

void SolverController::onWidthChanged(int width)
{
    _size.width = width;
}

void SolverController::onDepthChanged(int depth)
{
    _size.depth = depth;
}

void SolverController::onHeightChanged(int height)
{
    _size.height = height;
}

void SolverController::onConstraintsChanged(Constraints constraints)
{
    _constraints = constraints;
}

void SolverController::onSolve()
{
    assert(_size == _constraints.size());
    auto maze = Maze(_size);
    auto solution = Solver::solve(maze, _constraints);

    emit solutionReady(solution);
}
