#ifndef CONSTRAINTROOMITEM_H
#define CONSTRAINTROOMITEM_H

#include "lib/basetypes.h"
#include "lib/optional.h"
#include "ui_types.h"

#include <QPainter>
#include <QGraphicsItem>

#include <map>
#include <set>

class ConstraintRoomItem : public QGraphicsItem
{
public:
    enum { Type = UserType + 3 };

    ConstraintRoomItem(Position position, qreal size, qreal lineWidth, qreal offset);

    void setConstraints(std::map<Direction, WallConstraint> constraints);

    void setGroups(std::set<ConnectionGroup> groups);

    QRectF boundingRect() const override;

    // overriding paint()
    void paint(QPainter * painter,
               const QStyleOptionGraphicsItem * option,
               QWidget * widget) override;

    int type() const override { return Type; }

    Position position() const { return _position; }

    std::map<Direction, WallConstraint> constraints() const { return _constraints; }

    std::set<ConnectionGroup> groups() const;

    void onMousePress(QGraphicsSceneMouseEvent *event);

    void onClearPress(QGraphicsSceneMouseEvent *event);

private:

    tl::optional<std::pair<QPointF, QPointF>> getLine(Direction direction) const;

    Position _position;
    std::map<ConnectionGroup, QRectF> _groups;
    std::map<Direction, WallConstraint> _constraints;
    std::map<WallConstraint, QPen> _constraintPens;
    std::map<Direction, QRectF> _clickAreas;
    qreal _size;
    qreal _offset;
};

#endif // CONSTRAINTROOMITEM_H
