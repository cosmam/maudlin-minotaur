#ifndef MAZEROOMITEM_H
#define MAZEROOMITEM_H

#include "lib/basetypes.h"
#include "lib/optional.h"

#include <QPainter>
#include <QGraphicsItem>

#include <unordered_set>

class MazeRoomItem : public QGraphicsItem
{
public:
    MazeRoomItem(qreal size, qreal lineWidth, qreal offset);

    void setData(std::unordered_set<Direction> walls, RoomState state);

    QRectF boundingRect() const override;

    // overriding paint()
    void paint(QPainter * painter,
               const QStyleOptionGraphicsItem * option,
               QWidget * widget) override;

private:

    tl::optional<std::pair<QPointF, QPointF>> getLine(Direction direction) const;

    std::unordered_set<Direction> _walls;
    std::map<bool, QPen> _constraintPens;
    RoomState _state;
    qreal _size;
    qreal _lineWidth;
    qreal _offset;
};


#endif // MAZEROOMITEM_H
