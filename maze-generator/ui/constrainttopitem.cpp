#include "constrainttopitem.h"

#include <QColor>
#include <QGraphicsSceneMouseEvent>

namespace {

const std::vector<Direction> SideWalls = {Direction::North, Direction::East, Direction::South, Direction::West};

WallConstraint cycle(WallConstraint constraint) {
    switch(constraint) {
    case WallConstraint::Unconstrained:
        return WallConstraint::Exists;
    case WallConstraint::Exists:
        return WallConstraint::Removed;
    case WallConstraint::Removed:
        return WallConstraint::Unconstrained;
    }
}
}

ConstraintTopItem::ConstraintTopItem(Position position, qreal size, qreal lineWidth, qreal offset)
    : QGraphicsItem(),
      _position(position),
      _constraint(WallConstraint::Unconstrained),
      _constraintPens({{WallConstraint::Unconstrained, QPen(QBrush(QColorConstants::Gray), lineWidth, Qt::DotLine)},
{WallConstraint::Exists, QPen(QBrush(QColorConstants::Black), lineWidth)},
{WallConstraint::Removed, QPen(QBrush(QColorConstants::Black), 0.0)}}),
      _size(size),
      _offset(offset),
      _clickArea(QRectF(QPointF(_offset * 1.5, _offset * 1.5), QPointF(_size - 1.5 * _offset, _size - 1.5 * _offset)))
{

}

void ConstraintTopItem::setConstraint(WallConstraint constraint)
{
    _constraint = constraint;
}

void ConstraintTopItem::setGroups(std::set<ConnectionGroup> groups) {
    _groups = groups;
}

QRectF ConstraintTopItem::boundingRect() const
{
    // outer most edges
    return QRectF(0,0,_size,_size);
}

void ConstraintTopItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QRectF rect = boundingRect();

    painter->setPen(QPen(Qt::NoPen));
    if(_groups.size() > 0) {
        QRectF childRect(rect.x(), rect.y(), rect.width()/_groups.size(), rect.height());
        for(auto group : _groups) {
            painter->setBrush(QBrush(getColor(group)));
            painter->drawRect(childRect);
            childRect.moveLeft(childRect.x() + childRect.width());
        }
    } else {
        painter->setBrush(QBrush(QColor(222,222,222)));
        painter->drawRect(rect);
    }

    painter->setPen(QPen(QColorConstants::Black, 0.0));
    painter->setBrush(QBrush(QColorConstants::Transparent));
    painter->drawRect(rect);

    for(auto wall : SideWalls) {
        auto points = getLine(wall);
        if(points.has_value()) {
            painter->setPen(_constraintPens[WallConstraint::Removed]);
            painter->drawLine(points->first, points->second);
        }
    }

    painter->setPen(_constraintPens[_constraint]);
    painter->setBrush(_constraint == WallConstraint::Removed ? QColorConstants::White : QColor(222,222,222));
    QRectF inset(rect.topLeft().rx() + _offset, rect.topLeft().ry() + _offset, rect.width() - 2.0 * _offset, rect.height() - 2.0 * _offset);
    painter->drawEllipse(inset);
}

tl::optional<std::pair<QPointF, QPointF>> ConstraintTopItem::getLine(Direction direction) const
{
    tl::optional<std::pair<QPointF, QPointF>> points;
    const qreal low = 0.0 + _constraintPens.at(WallConstraint::Removed).widthF()/2.0;
    const qreal high = _size - low;

    switch(direction) {
    case Direction::North:
        points = std::pair<QPointF, QPointF>{QPointF(low, high), QPointF(high, high)};
        break;
    case Direction::East:
        points = std::pair<QPointF, QPointF>{QPointF(high, low), QPointF(high, high)};
        break;
    case Direction::South:
        points = std::pair<QPointF, QPointF>{QPointF(low, low), QPointF(high, low)};
        break;
    case Direction::West:
        points = std::pair<QPointF, QPointF>{QPointF(low, low), QPointF(low, high)};
        break;
    case Direction::Top:    // intentional fall-through
    case Direction::Bottom:
        break;
    }

    return points;
}

void ConstraintTopItem::onMousePress(QGraphicsSceneMouseEvent *event) {
    auto pos = mapFromScene(event->scenePos());
    if(_clickArea.contains(pos)) {
        _constraint = cycle(_constraint);
    }
}
