#include "constraintscontroller.h"

#include "lib/basetypes.h"

#include <QGraphicsView>

#include <set>

ConstraintsController::ConstraintsController(QGraphicsScene * scene)
    : _scene(scene),
      _constraints(Constraints(Size{1, 1, 1})),
      _size{1,1,1}, _isActive(false),
      _mazeType(MazeType::Two_Dimensional)
{}

void ConstraintsController::setActive(bool isActive) {
    auto isChanged = (_isActive == isActive);
    _isActive = isActive;

    if(isChanged && _isActive) {     // newly activated
        displayConstraints();
    } else if(isChanged) {           // newly deactivated
        clearDisplay();
    }
}

void ConstraintsController::onMazeTypeChanged(QString mazeTypeString) {
}

void ConstraintsController::onWidthChanged(int width) {
    _size.width = width;
}

void ConstraintsController::onDepthChanged(int depth) {
    _size.depth = depth;
}

void ConstraintsController::onHeightChanged(int height) {
    _size.height = height;
    _mazeType = (height > 1 ? MazeType::Three_Dimensional : _mazeType);
}

void ConstraintsController::onStart() {
    _constraints = Constraints(_size);
    clearDisplay();
    displayConstraints();
}

void ConstraintsController::onSolve() {
    emit constraintsChanged(_constraints);
}

void ConstraintsController::onConstraintChanged(Position position, std::map<Direction, WallConstraint> constraints) {
    for(auto it = constraints.begin() ; it != constraints.end() ; ++it) {
        _constraints.setConstraint(position, it->first, it->second);
    }
    for(auto direction : getDirections(_constraints.size(), position)) {
        auto otherPos = calculatePosition(position, direction);
        _rooms[otherPos]->setConstraints(getConstraints(otherPos));
    }
    _scene->update();
}

void ConstraintsController::onGroupsChanged() {
    auto connections = getConnections();

    std::vector<std::pair<Position, Position>> pairs;
    for(auto it = connections.begin() ; it != connections.end() ; ++it) {
        auto v = generatePairs(it->second);
        pairs.insert(pairs.end(), v.begin(), v.end());
    }

    _constraints.setConnections(pairs);
    _scene->update();
}

void ConstraintsController::clearDisplay() {
    _scene->clear();
    _rooms.clear();
    _tops.clear();

    _scene->update();
}

void ConstraintsController::displayConstraints() {
    if(!_isActive) {
        return;
    }

    int levels = _constraints.size().height + (_mazeType == MazeType::Three_Dimensional ? 1 : 0);
    auto columns = getColumns(levels);
    int width = (int)_constraints.size().width;
    int depth = (int)_constraints.size().depth;
    auto height = _constraints.size().height;

    QPointF topLeft(-1.0 * ROOM_SIZE, -1.0 * ROOM_SIZE);
    QPointF bottomRight(0.0, 0.0);

    for(auto position : generatePositions(_constraints.size())) {
        auto room = new ConstraintRoomItem(position, ROOM_SIZE, LINE_WIDTH, OFFSET);
        room->setConstraints(getConstraints(position));
        _scene->addItem(room);
        room->setPos(getRoomPos(position, columns, width, depth));
        _rooms[position] = room;

        bottomRight.setX(qMax(bottomRight.x(), room->sceneBoundingRect().x()));
        bottomRight.setY(qMax(bottomRight.y(), room->sceneBoundingRect().y()));

        if(((_mazeType == MazeType::Three_Dimensional) || (height > 1)) &&
                (position.z == (height-1))) {    // add the top layer for a 3D maze
            auto mazeTop = new ConstraintTopItem(position, ROOM_SIZE, LINE_WIDTH, OFFSET);
            mazeTop->setConstraint(_constraints.getConstraint(position, Direction::Top));
            _scene->addItem(mazeTop);
            mazeTop->setPos(getRoomPos(Position{position.x, position.y, position.z+1}, columns, width, depth));
            _tops[position] = mazeTop;
        }
    }

    QFont font;
    font.setPointSizeF(0.2 * ROOM_SIZE);
    for(unsigned int i=0 ; i <= height ; ++i) {
        QString text(i == height ? "Top" : "Level " + QString::number(i+1));
        auto textItem = _scene->addSimpleText(text, font);
        auto pos = getRoomPos(Position{0,0,i}, columns, width, depth);
        textItem->setPos(QPointF(pos.x() + 0.5 * (ROOM_SIZE * (qreal)width - textItem->boundingRect().width()),
                                 pos.y() - textItem->boundingRect().height()));
    }

    bottomRight.setX(bottomRight.x() + 2.0 * ROOM_SIZE);
    bottomRight.setY(bottomRight.y() + 2.0 * ROOM_SIZE);

    _scene->setSceneRect(QRectF(topLeft, bottomRight));
    _scene->update();

    for(auto view : _scene->views()) {
        view->fitInView(QRectF(topLeft, bottomRight), Qt::KeepAspectRatio);
    }
}

std::map<ConnectionGroup, std::set<Position>> ConstraintsController::getConnections() const {
    std::map<ConnectionGroup, std::set<Position>> connections;

    for(auto rit = _rooms.begin() ; rit != _rooms.end() ; ++rit) {
        auto groups = rit->second->groups();
        for(auto git = groups.begin() ; git != groups.end() ; ++git) {
            if(connections.count(*git) == 0) {
                connections[*git] = std::set<Position>{rit->second->position()};
            } else {
                connections[*git].insert(rit->second->position());
            }
        }
    }

    return connections;
}

std::map<Direction, WallConstraint> ConstraintsController::getConstraints(Position position) const {
    std::map<Direction, WallConstraint> constraints;

    for(auto direction : AllDirections) {
        constraints[direction] = _constraints.getConstraint(position, direction);
    }

    return constraints;
}
