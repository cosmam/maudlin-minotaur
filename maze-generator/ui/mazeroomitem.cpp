#include "mazeroomitem.h"

#include <QColor>

namespace {
const QMap<RoomState, QColor> _stateColors = {{RoomState::Visited, QColor(222,222,222)},
                                              {RoomState::Unvisited, QColorConstants::DarkGray},
                                              {RoomState::OnPath, QColorConstants::DarkRed},
                                              {RoomState::Current, QColorConstants::Red}};
}

MazeRoomItem::MazeRoomItem(qreal size, qreal lineWidth, qreal offset)
    : QGraphicsItem(),
      _walls(AllDirections),
      _constraintPens{{true, QPen(QColorConstants::Black, lineWidth)},
                      {false, QPen(QColorConstants::Gray, 0.0)}},
      _state(RoomState::Unvisited),
      _size(size),
      _lineWidth(lineWidth),
      _offset(offset)
{
}

void MazeRoomItem::setData(std::unordered_set<Direction> walls, RoomState state)
{
    _walls = walls;
    _state = state;
}

QRectF MazeRoomItem::boundingRect() const
{
    // outer most edges
    return QRectF(0,0,_size,_size);
}

void MazeRoomItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QRectF rect = boundingRect();

    painter->setPen(QPen(QColorConstants::Gray, 0.0));
    painter->setBrush(QBrush(_stateColors.value(_state)));
    painter->drawRect(rect);

    for(auto direction : AllDirections) {
        auto points = getLine(direction);
        if(points.has_value()) {
            painter->setPen(_constraintPens.at(_walls.count(direction) > 0));
            painter->drawLine(points->first, points->second);
        }
    }

    // there is no bottom wall, so we show a circular cutout
    if(_walls.count(Direction::Bottom) == 0) {
        painter->setPen(QPen(QColorConstants::Black, _lineWidth));
        painter->setBrush(QColorConstants::White);
        QRectF inset(rect.topLeft().rx() + _offset, rect.topLeft().ry() + _offset, rect.width() - 2.0 * _offset, rect.height() - 2.0 * _offset);
        painter->drawEllipse(inset);
    }
}

tl::optional<std::pair<QPointF, QPointF>> MazeRoomItem::getLine(Direction direction) const
{
    tl::optional<std::pair<QPointF, QPointF>> points;
    const qreal low = 0.0 + _constraintPens.at(_walls.count(direction) > 0).widthF()/2.0;
    const qreal high = _size - low;

    switch(direction) {
    case Direction::North:
        points = std::pair<QPointF, QPointF>{QPointF(low, high), QPointF(high, high)};
        break;
    case Direction::East:
        points = std::pair<QPointF, QPointF>{QPointF(high, low), QPointF(high, high)};
        break;
    case Direction::South:
        points = std::pair<QPointF, QPointF>{QPointF(low, low), QPointF(high, low)};
        break;
    case Direction::West:
        points = std::pair<QPointF, QPointF>{QPointF(low, low), QPointF(low, high)};
        break;
    case Direction::Top:    // intentional fall-through
    case Direction::Bottom:
        break;
    }

    return points;
}
