#ifndef UI_TYPES_H
#define UI_TYPES_H

#include "lib/basetypes.h"

#include <QColor>
#include <QMap>
#include <QMetaType>
#include <QPointF>
#include <QString>

const qreal ROOM_SIZE = 80.0;
const qreal LINE_WIDTH = 2.0;
const qreal OFFSET = 16.0;

enum class ConnectionGroup {
    Maroon,
    Red,
    Pink,
    Lavendar,
    Magenta,
    Purple,
    Blue,
    Teal,
    Cyan,
    Mint,
    Lime,
    Green,
    Olive,
    Yellow,
    Beige,
    Coral,
    Orange,
    Brown
};
Q_DECLARE_METATYPE(ConnectionGroup);

const std::set<ConnectionGroup> AllGroups{ConnectionGroup::Maroon,
            ConnectionGroup::Red,
            ConnectionGroup::Pink,
            ConnectionGroup::Lavendar,
            ConnectionGroup::Magenta,
            ConnectionGroup::Purple,
            ConnectionGroup::Blue,
            ConnectionGroup::Teal,
            ConnectionGroup::Cyan,
            ConnectionGroup::Mint,
            ConnectionGroup::Lime,
            ConnectionGroup::Green,
            ConnectionGroup::Olive,
            ConnectionGroup::Yellow,
            ConnectionGroup::Beige,
            ConnectionGroup::Coral,
            ConnectionGroup::Orange,
            ConnectionGroup::Brown};

QColor getColor(ConnectionGroup group);

QString getString(ConnectionGroup group);

QString getString(MazeType mazeType);

MazeType getMazeType(QString mazeTypeString);

int getColumns(int levels);

QPointF getRoomPos(Position position, int columns, int width, int depth);

#endif // UI_TYPES_H
