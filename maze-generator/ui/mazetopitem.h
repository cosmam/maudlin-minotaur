#ifndef MAZETOPITEM_H
#define MAZETOPITEM_H

#include <QPainter>
#include <QGraphicsItem>

class MazeTopItem : public QGraphicsItem
{
public:
    MazeTopItem(qreal size, qreal lineWidth, qreal offset);

    void setData(bool isOpen);

    QRectF boundingRect() const;

    // overriding paint()
    void paint(QPainter * painter,
               const QStyleOptionGraphicsItem * option,
               QWidget * widget);

private:

    bool _isOpen;
    qreal _size;
    qreal _lineWidth;
    qreal _offset;

};

#endif // MAZETOPITEM_H
