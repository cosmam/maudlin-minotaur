#ifndef CONSTRAINTSCONTROLLER_H
#define CONSTRAINTSCONTROLLER_H

#include "constraintroomitem.h"
#include "constrainttopitem.h"
#include "lib/constraints.h"

#include <QGraphicsScene>
#include <QObject>
#include <QPointer>

class ConstraintsController : public QObject
{
    Q_OBJECT

public:
    ConstraintsController(QGraphicsScene * scene);

    void setActive(bool isActive);

    bool isActive() const { return _isActive; }

public slots:

    void onMazeTypeChanged(QString mazeTypeString);

    void onWidthChanged(int width);

    void onDepthChanged(int depth);

    void onHeightChanged(int height);

    void onStart();

    void onSolve();

    void onConstraintChanged(Position position, std::map<Direction, WallConstraint> constraints);

    void onGroupsChanged();

signals:

    void constraintsChanged(Constraints constraints);

private:

    void clearDisplay();

    void displayConstraints();

    void update();

    std::map<ConnectionGroup, std::set<Position>> getConnections() const;

    std::map<Direction, WallConstraint> getConstraints(Position position) const;

    QPointer<QGraphicsScene> _scene;
    Constraints _constraints;
    std::map<Position, ConstraintRoomItem *> _rooms;
    std::map<Position, ConstraintTopItem *> _tops;
    Size _size;
    bool _isActive;
    MazeType _mazeType;

};

#endif // CONSTRAINTSCONTROLLER_H
