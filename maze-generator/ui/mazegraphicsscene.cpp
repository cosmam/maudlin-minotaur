#include "mazegraphicsscene.h"

#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>

#include <map>

MazeGraphicsScene::MazeGraphicsScene(QObject * parent)
    : QGraphicsScene(parent),
      _clearSelected(false)
{}

void MazeGraphicsScene::setGroupSelector(ConnectionGroup group) {
    _group = group;
}

void MazeGraphicsScene::onClearSelected()
{
    _clearSelected = true;
}

void MazeGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    for(auto item : getConstraintRoomItems(event->scenePos())) {
        if(_clearSelected) {
            item->onClearPress(event);
            emit groupsChanged();
            _clearSelected = false;
        } else if(_group.has_value()) {
            auto groups = item->groups();
            groups.insert(*_group);
            item->setGroups(groups);
            emit groupsChanged();
        } else {
            auto constraints = item->constraints();
            item->onMousePress(event);
            if(constraints != item->constraints()) {
                emit constraintChanged(item->position(), item->constraints());
            }
        }
    }

    for(auto item : getConstraintTopItems(event->scenePos())) {
        auto constraint = item->constraint();
        item->onMousePress(event);
        if(constraint != item->constraint()) {
            std::map<Direction, WallConstraint> constraints;
            constraints[Direction::Top] = item->constraint();
            emit constraintChanged(item->position(), constraints);
        }
    }
    _group.reset();
}

QList<ConstraintRoomItem *> MazeGraphicsScene::getConstraintRoomItems(QPointF pos) const {
    QList<ConstraintRoomItem *> roomItems;

    for(auto item : items(pos)) {
        auto constraintItem = qgraphicsitem_cast<ConstraintRoomItem *>(item);
        if(constraintItem) {
            roomItems.append(constraintItem);
        }
    }

    return roomItems;
}

QList<ConstraintTopItem *> MazeGraphicsScene::getConstraintTopItems(QPointF pos) const
{
    QList<ConstraintTopItem *> topItems;

    for(auto item : items(pos)) {
        auto constraintItem = qgraphicsitem_cast<ConstraintTopItem *>(item);
        if(constraintItem) {
            topItems.append(constraintItem);
        }
    }

    return topItems;
}
