
#include "constraintroomitem.h"

#include <QColor>
#include <QGraphicsSceneMouseEvent>

namespace {

const std::set<Direction> drawnRemoved{Direction::West, Direction::North, Direction::Bottom};

bool drawConstraint(Direction direction, WallConstraint constraint) {
    return constraint != WallConstraint::Removed || (drawnRemoved.count(direction) > 0);
}

WallConstraint cycle(WallConstraint constraint) {
    switch(constraint) {
    case WallConstraint::Unconstrained:
        return WallConstraint::Exists;
    case WallConstraint::Exists:
        return WallConstraint::Removed;
    case WallConstraint::Removed:
        return WallConstraint::Unconstrained;
    }
}

std::set<ConnectionGroup> getGroups(const std::map<ConnectionGroup, QRectF> & groupMap) {
    std::set<ConnectionGroup> groups;
    for(auto it = groupMap.begin() ; it != groupMap.end() ; ++it) {
        groups.insert(it->first);
    }
    return groups;
}

}

ConstraintRoomItem::ConstraintRoomItem(Position position, qreal size, qreal lineWidth, qreal offset)
    : QGraphicsItem(),
      _position(position),
      _constraintPens({{WallConstraint::Unconstrained, QPen(QBrush(QColorConstants::Gray), lineWidth, Qt::DashLine)},
{WallConstraint::Exists, QPen(QBrush(QColorConstants::Black), lineWidth)},
{WallConstraint::Removed, QPen(QBrush(QColorConstants::LightGray), 0.0)}}),
      _size(size),
      _offset(offset)
{
    qreal low = _offset;
    qreal high = _size - offset;

    _clickAreas[Direction::West] = QRectF(QPointF(0, low), QPointF(low, high));
    _clickAreas[Direction::South] = QRectF(QPointF(low, 0), QPointF(high, low));
    _clickAreas[Direction::East] = QRectF(QPointF(high, low), QPointF(_size, high));
    _clickAreas[Direction::North] = QRectF(QPointF(low, high), QPointF(high, _size));
    _clickAreas[Direction::Bottom] = QRectF(QPointF(_offset * 1.5, _offset * 1.5), QPointF(_size - 1.5 * _offset, _size - 1.5 * _offset));
    _clickAreas[Direction::Top] = QRectF(0,0,0,0);
}

void ConstraintRoomItem::setConstraints(std::map<Direction, WallConstraint> constraints)
{
    _constraints = constraints;
}

void ConstraintRoomItem::setGroups(std::set<ConnectionGroup> groups) {
    QRectF rect = boundingRect();
    QRectF childRect(rect.x(), rect.y(), rect.width()/groups.size(), rect.height());
    _groups.clear();
    for(auto group : groups) {
        _groups[group] = childRect;
        childRect.moveLeft(childRect.x() + childRect.width());
    }
}

QRectF ConstraintRoomItem::boundingRect() const
{
    // outer most edges
    return QRectF(0,0,_size,_size);
}

void ConstraintRoomItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QRectF rect = boundingRect();

    painter->setPen(QPen(Qt::NoPen));
    if(_groups.size() > 0) {
        for(auto it = _groups.begin() ; it != _groups.end() ; ++it) {
            painter->setBrush(QBrush(getColor(it->first)));
            painter->drawRect(it->second);
        }
    } else {
        painter->setBrush(QBrush(QColor(222,222,222)));
        painter->drawRect(rect);
    }

    painter->setPen(QPen(QColorConstants::Gray, 0.0));
    painter->setBrush(QBrush(QColorConstants::Transparent));
    painter->drawRect(rect);

    for(auto it = _constraints.begin() ; it != _constraints.end() ; ++it) {
        auto points = getLine(it->first);
        if(points.has_value() && drawConstraint(it->first, it->second)) {
            painter->setPen(_constraintPens[it->second]);
            painter->drawLine(points->first, points->second);
        }
    }

    if(_constraints.count(Direction::Bottom) > 0) {
        painter->setPen(_constraintPens[_constraints[Direction::Bottom]]);
        painter->setBrush(_constraints[Direction::Bottom] == WallConstraint::Removed ? QColorConstants::White : QColorConstants::Transparent);
        QRectF inset(rect.topLeft().rx() + _offset, rect.topLeft().ry() + _offset, rect.width() - 2.0 * _offset, rect.height() - 2.0 * _offset);
        painter->drawEllipse(inset);
    }
}

std::set<ConnectionGroup> ConstraintRoomItem::groups() const
{
    return getGroups(_groups);
}

tl::optional<std::pair<QPointF, QPointF>> ConstraintRoomItem::getLine(Direction direction) const
{
    tl::optional<std::pair<QPointF, QPointF>> points;
    const qreal low = 0.0 + _constraintPens.at(_constraints.at(direction)).widthF()/2.0;
    const qreal high = _size - low;

    switch(direction) {
    case Direction::North:
        points = std::pair<QPointF, QPointF>{QPointF(low, high), QPointF(high, high)};
        break;
    case Direction::East:
        points = std::pair<QPointF, QPointF>{QPointF(high, low), QPointF(high, high)};
        break;
    case Direction::South:
        points = std::pair<QPointF, QPointF>{QPointF(low, low), QPointF(high, low)};
        break;
    case Direction::West:
        points = std::pair<QPointF, QPointF>{QPointF(low, low), QPointF(low, high)};
        break;
    case Direction::Top:    // intentional fall-through
    case Direction::Bottom:
        break;
    }

    return points;
}

void ConstraintRoomItem::onMousePress(QGraphicsSceneMouseEvent *event) {
    auto pos = mapFromScene(event->scenePos());
    for(auto it = _constraints.begin() ; it != _constraints.end() ; ++it) {
        if(_clickAreas.at(it->first).contains(pos)) {
            it->second = cycle(it->second);
        }
    }
}

void ConstraintRoomItem::onClearPress(QGraphicsSceneMouseEvent *event)
{
    auto groups = _groups;
    auto pos = mapFromScene(event->scenePos());
    for(auto it = groups.begin() ; it != groups.end() ; ++it) {
        if(it->second.contains(pos)) {
            groups.erase(it);
            setGroups(getGroups(groups));
            break;
        }
    }
}
