#ifndef MAZEGRAPHICSSCENE_H
#define MAZEGRAPHICSSCENE_H

#include "constraintroomitem.h"
#include "constrainttopitem.h"
#include "lib/optional.h"
#include "ui_types.h"

#include <QGraphicsView>
#include <QList>

class MazeGraphicsScene : public QGraphicsScene
{
    Q_OBJECT
public:
    MazeGraphicsScene(QObject * parent = nullptr);

    void setGroupSelector(ConnectionGroup group);

public slots:

    void onClearSelected();

signals:

    void constraintChanged(Position position, std::map<Direction, WallConstraint> constraints);

    void groupsChanged();

protected:

    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

private:

    QList<ConstraintRoomItem *> getConstraintRoomItems(QPointF pos) const;

    QList<ConstraintTopItem *> getConstraintTopItems(QPointF pos) const;

    tl::optional<ConnectionGroup> _group;

    bool _clearSelected;
};

#endif // CONSTRAINTROOMITEM_H
