#ifndef CONSTRAINTTOPITEM_H
#define CONSTRAINTTOPITEM_H

#include "lib/basetypes.h"
#include "lib/optional.h"
#include "ui_types.h"

#include <QPainter>
#include <QGraphicsItem>

#include <map>
#include <set>

class ConstraintTopItem : public QGraphicsItem
{
public:
    enum { Type = UserType + 4 };

    ConstraintTopItem(Position position, qreal size, qreal lineWidth, qreal offset);

    void setConstraint(WallConstraint constraint);

    void setGroups(std::set<ConnectionGroup> groups);

    QRectF boundingRect() const override;

    // overriding paint()
    void paint(QPainter * painter,
               const QStyleOptionGraphicsItem * option,
               QWidget * widget) override;

    int type() const override { return Type; }

    Position position() const { return _position; }

    WallConstraint constraint() const { return _constraint; }

    std::set<ConnectionGroup> groups() const { return _groups; }

    void onMousePress(QGraphicsSceneMouseEvent *event);

private:

    tl::optional<std::pair<QPointF, QPointF>> getLine(Direction direction) const;

    Position _position;
    std::set<ConnectionGroup> _groups;
    WallConstraint _constraint;
    std::map<WallConstraint, QPen> _constraintPens;
    qreal _size;
    qreal _offset;
    QRectF _clickArea;
};

#endif // CONSTRAINTTOPITEM_H
