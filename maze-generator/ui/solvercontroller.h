#ifndef SOLVERCONTROLLER_H
#define SOLVERCONTROLLER_H

#include "lib/constraints.h"
#include "lib/solver.h"

#include <QObject>

class SolverController : public QObject
{
    Q_OBJECT
public:
    explicit SolverController(QObject *parent = nullptr);

public slots:

    void onWidthChanged(int width);

    void onDepthChanged(int depth);

    void onHeightChanged(int height);

    void onConstraintsChanged(Constraints constraints);

    void onSolve();

signals:

    void solutionReady(Solver::Solution solution);

private:

    Size _size;
    Constraints _constraints;
};

#endif // SOLVERCONTROLLER_H
