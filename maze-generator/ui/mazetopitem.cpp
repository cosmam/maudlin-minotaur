#include "mazetopitem.h"

MazeTopItem::MazeTopItem(qreal size, qreal lineWidth, qreal offset)
    : _isOpen(false),
      _size(size),
      _lineWidth(lineWidth),
      _offset(offset)
{
}

void MazeTopItem::setData(bool isOpen)
{
    _isOpen = isOpen;
}

QRectF MazeTopItem::boundingRect() const
{
    // outer most edges
    return QRectF(0,0,_size,_size);
}

void MazeTopItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QRectF rect = boundingRect();

    painter->setPen(QPen(QColorConstants::Black, 0.0));
    painter->setBrush(QBrush(QColorConstants::LightGray));
    painter->drawRect(rect);

    // there is no bottom wall, so we show a circular cutout
    if(_isOpen) {
        painter->setPen(QPen(QColorConstants::Black, _lineWidth));
        painter->setBrush(QColorConstants::White);
        QRectF inset(rect.bottomLeft().rx() + _offset, rect.bottomLeft().ry() + _offset, rect.width() - 2.0 * _offset, rect.height() - 2.0 * _offset);
        painter->drawEllipse(inset);
    }
}
