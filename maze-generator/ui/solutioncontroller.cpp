#include "solutioncontroller.h"

#include "ui_types.h"

#include <QGraphicsView>

namespace {
int delay = 200;

}

SolutionController::SolutionController(QGraphicsScene * scene)
    : _scene(scene),
      _timer(new QTimer(this)),
      _solution({}),
      _isActive(false),
      _mazeType(MazeType::Two_Dimensional)
{
    _timer->setSingleShot(false);
    _timer->setInterval(delay);
    _timer->callOnTimeout(this, &SolutionController::step);
}

void SolutionController::setActive(bool isActive) {
    auto isChanged = (_isActive == isActive);
    _isActive = isActive;
    
    if(isChanged && _isActive) {     // newly activated
        displayMaze();
    } else if(isChanged) {           // newly deactivated
        clearDisplay();
    }
}

void SolutionController::onMazeTypeChanged(QString mazeTypeString) {
    _mazeType = getMazeType(mazeTypeString);
    displayMaze();
}

void SolutionController::displaySolution(Solver::Solution solution) {
    _solution = solution;
    onReset();
}

void SolutionController::onReset() {
    _timer->stop();
    _stepperGoal.reset();

    if(_isActive && _solution.has_value()) {
        clearDisplay();
        displayMaze();
    }
}

void SolutionController::onStep() {
    if(!_isActive || !_solution.has_value() || _stepper.getState() == SteppingState::Finished) {
        return;
    }

    _stepperGoal.reset();
    _timer->stop();

    step();
}

void SolutionController::onWatch() {
    if(!_isActive || !_solution.has_value() || _stepper.getState() == SteppingState::Finished) {
        return;
    }

    switch(_stepper.getState()) {
    case SteppingState::Initial:			// intentional fall-through
    case SteppingState::Start_Of_Find_Path: // intentional fall-through
    case SteppingState::Stepping_Find:		// intentional fall-through
    case SteppingState::End_Of_Visit_Path:
        _stepperGoal = SteppingState::End_Of_Find_Path;
        break;
    case SteppingState::End_Of_Find_Path:	// intentional fall-through
    case SteppingState::Stepping_Visit:
        _stepperGoal = SteppingState::End_Of_Visit_Path;
        break;
    case SteppingState::Finished:			// technically impossible, but a sane choice
        _stepperGoal.reset();
        break;
    }

    _timer->start();
    step();
}

void SolutionController::onRun() {
    if(!_isActive || !_solution.has_value() || _stepper.getState() == SteppingState::Finished) {
        return;
    }

    _stepperGoal = SteppingState::Finished;
    _timer->start();
    step();
}

void SolutionController::onResult() {
    if(!_isActive || !_solution.has_value() || _stepper.getState() == SteppingState::Finished) {
        return;
    }

    _stepperGoal.reset();
    _timer->stop();
    
    auto rooms = _stepper.finish();
    for(auto room : rooms) {
        auto roomItem = _rooms[room.position];
        roomItem->setData(room.walls, room.state);
        _scene->invalidate(roomItem->sceneBoundingRect());
    }
}

void SolutionController::step() {
    auto rooms = _stepper.step();
    for(auto room : rooms) {
        auto roomItem = _rooms[room.position];
        roomItem->setData(room.walls, room.state);
        _scene->invalidate(roomItem->sceneBoundingRect());
    }

    if(_stepperGoal.has_value() && _stepper.getState() == *_stepperGoal) {
        _stepperGoal.reset();
        _timer->stop();
    }
}

void SolutionController::clearDisplay() {
    _scene->clear();
    _rooms.clear();
    _tops.clear();

    _scene->update();
}

void SolutionController::displayMaze() {
    if(!_isActive || !_solution.has_value()) {
        return;
    }

    auto rooms = _stepper.setSolution(*_solution);
    int levels = _solution->builder.getMaze().size().height + (_mazeType == MazeType::Three_Dimensional ? 1 : 0);
    auto columns = getColumns(levels);
    int width = (int)_solution->builder.getMaze().size().width;
    int depth = (int)_solution->builder.getMaze().size().depth;
    auto height = _solution->builder.getMaze().size().height;

    QPointF topLeft(-1.0 * ROOM_SIZE, -1.0 * ROOM_SIZE);
    QPointF bottomRight(0.0, 0.0);

    for(auto room : rooms) {
        auto mazeRoom = new MazeRoomItem(ROOM_SIZE, LINE_WIDTH, OFFSET);
        mazeRoom->setData(room.walls, room.state);
        _scene->addItem(mazeRoom);
        mazeRoom->setPos(getRoomPos(room.position, columns, width, depth));
        _rooms[room.position] = mazeRoom;

        bottomRight.setX(qMax(bottomRight.x(), mazeRoom->sceneBoundingRect().x()));
        bottomRight.setY(qMax(bottomRight.y(), mazeRoom->sceneBoundingRect().y()));

        if(((_mazeType == MazeType::Three_Dimensional) || (_solution->builder.getMaze().size().height > 1)) &&
                (room.position.z == (height-1))) {    // add the top layer for a 3D maze
            auto mazeTop = new MazeTopItem(ROOM_SIZE, LINE_WIDTH, OFFSET);
            mazeTop->setData(room.walls.count(Direction::Top) == 0);
            _scene->addItem(mazeTop);
            mazeTop->setPos(getRoomPos(Position{room.position.x, room.position.y, room.position.z+1}, columns, width, depth));
            _tops[room.position] = mazeTop;
        }
    }

    QFont font;
    font.setPointSizeF(0.2 * ROOM_SIZE);
    for(unsigned int i=0 ; i <= height ; ++i) {
        QString text(i == height ? "Top" : "Level " + QString::number(i+1));
        auto textItem = _scene->addSimpleText(text, font);
        auto pos = getRoomPos(Position{0,0,i}, columns, width, depth);
        textItem->setPos(QPointF(pos.x() + 0.5 * (ROOM_SIZE * (qreal)width - textItem->boundingRect().width()),
                                 pos.y() - textItem->boundingRect().height()));
    }

    bottomRight.setX(bottomRight.x() + 2.0 * ROOM_SIZE);
    bottomRight.setY(bottomRight.y() + 2.0 * ROOM_SIZE);

    _scene->setSceneRect(QRectF(topLeft, bottomRight));
    _scene->update();
    
    for(auto view : _scene->views()) {
        view->fitInView(QRectF(topLeft, bottomRight), Qt::KeepAspectRatio);
    }
}
