#include "ui_types.h"

#include <QPointF>

#include <cmath>

namespace {

QMap<MazeType, QString> _enumNames{{MazeType::Two_Dimensional, "2D"}, {MazeType::Three_Dimensional, "3D"}};

QMap<ConnectionGroup, QPair<QColor, QString>> _enumColors{
    {ConnectionGroup::Maroon, QPair<QColor, QString>{QColor(136, 0, 0), "Maroon"}},
    {ConnectionGroup::Brown, QPair<QColor, QString>{QColor(154, 99, 36), "Brown"}},
    {ConnectionGroup::Olive, QPair<QColor, QString>{QColor(128, 128, 0), "Olive"}},
    {ConnectionGroup::Teal, QPair<QColor, QString>{QColor(70, 153, 144), "Teal"}},
    {ConnectionGroup::Red, QPair<QColor, QString>{QColor(230, 25, 75), "Red"}},
    {ConnectionGroup::Orange, QPair<QColor, QString>{QColor(245, 130, 49), "Orange"}},
    {ConnectionGroup::Yellow, QPair<QColor, QString>{QColor(255, 225, 25), "Yellow"}},
    {ConnectionGroup::Lime, QPair<QColor, QString>{QColor(191, 239, 69), "Lime"}},
    {ConnectionGroup::Green, QPair<QColor, QString>{QColor(60, 180, 75), "Green"}},
    {ConnectionGroup::Cyan, QPair<QColor, QString>{QColor(66, 212, 244), "Cyan"}},
    {ConnectionGroup::Blue, QPair<QColor, QString>{QColor(67, 99, 216), "Blue"}},
    {ConnectionGroup::Purple, QPair<QColor, QString>{QColor(145, 30, 180), "Purple"}},
    {ConnectionGroup::Magenta, QPair<QColor, QString>{QColor(240, 50, 230), "Magenta"}},
    {ConnectionGroup::Pink, QPair<QColor, QString>{QColor(250, 190, 212), "Pink"}},
    {ConnectionGroup::Coral, QPair<QColor, QString>{QColor(255, 216, 177), "Coral"}},
    {ConnectionGroup::Beige, QPair<QColor, QString>{QColor(255, 250, 200), "Beige"}},
    {ConnectionGroup::Mint, QPair<QColor, QString>{QColor(170, 255, 195), "Mint"}},
    {ConnectionGroup::Lavendar, QPair<QColor, QString>{QColor(220, 190, 255), "Lavendar"}}};
}

QColor getColor(ConnectionGroup group) {
    return _enumColors.value(group).first;
}

QString getString(ConnectionGroup group) {
    return _enumColors.value(group).second;
}

QString getString(MazeType mazeType) {
    return _enumNames.value(mazeType);
}

MazeType getMazeType(QString mazeTypeString) {
    return _enumNames.key(mazeTypeString);
}

int getColumns(int levels) {
    auto count = (double)levels;
    auto root = sqrt(count);
    return (int)ceil(root);
}

QPointF getRoomPos(Position position, int columns, int width, int depth) {
    int row = position.z / columns;
    int col = position.z - (row * columns);
    int x_pos = position.x + col * (width+1);   // +1 for spacing
    int y_pos = position.y + row * (depth+1);   // +1 for spacing

    return QPointF(qreal(x_pos) * ROOM_SIZE, qreal(y_pos) * ROOM_SIZE);
}
