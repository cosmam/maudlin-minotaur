#include "basetypes.h"

#include "optional.h"

#include <algorithm>

bool operator==(const Position & lhs, const Position & rhs)
{
    return ((lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z));
}

bool operator!=(const Position & lhs, const Position & rhs)
{
    return !(lhs == rhs);
}

bool operator<(const Position & lhs, const Position & rhs)
{
    if(lhs == rhs) {
        return false;
    } else if((lhs.x == rhs.x) && (lhs.y == rhs.y)) {
        return lhs.z < rhs.z;
    } else if(lhs.x == rhs.x) {
        return lhs.y < rhs.y;
    } else {
        return lhs.x < rhs.x;
    }
}

bool operator==(const Size & lhs, const Size & rhs)
{
    return ((lhs.width == rhs.width) &&
            (lhs.depth == rhs.depth) &&
            (lhs.height == rhs.height));
}

Position calculatePosition(Position position, Direction direction) {
    Position newPosition(position);

    switch(direction) {
    case Direction::North:
        newPosition.y++;
        break;
    case Direction::East:
        newPosition.x++;
        break;
    case Direction::South:
        newPosition.y--;
        break;
    case Direction::West:
        newPosition.x--;
        break;
    case Direction::Top:
        newPosition.z++;
        break;
    case Direction::Bottom:
        newPosition.z--;
        break;
    }

    return newPosition;
}

std::vector<std::pair<Position,Position>> generatePairs(const std::set<Position> & positions) {
    std::vector<std::pair<Position,Position>> pairs;
    if(positions.size() < 2) {
        return pairs;
    }

    std::string bitmask(2, 1);
    bitmask.resize(positions.size(), 0);

    tl::optional<Position> first, second;

    std::vector<Position> pos(positions.begin(), positions.end());
    // print integers and permute bitmask
    do {
        for (unsigned int i = 0; i < positions.size(); ++i) {
            if (bitmask[i] && first.has_value()) {
                second = pos[i];
            } else if(bitmask[i]) {
                first = pos[i];
            }
        }
        pairs.push_back(std::pair<Position,Position>{*first, *second});
    } while (std::prev_permutation(bitmask.begin(), bitmask.end()));

    return pairs;
}

std::vector<Position> generatePositions(Size size) {
    std::vector<Position> positions;

    for(unsigned int i=0; i < size.width ; ++i) {
        for(unsigned int j=0; j < size.depth ; ++j) {
            for(unsigned int k=0; k < size.height ; ++k) {
                positions.push_back(Position{i, j, k});
            }
        }
    }

    return positions;
}

std::vector<Direction> getDirections(Size size, Position pos)
{
    std::vector<Direction> directions;

    if (pos.x > 0) { directions.push_back(Direction::West); }
    if (pos.y > 0) { directions.push_back(Direction::South); }
    if (pos.z > 0) { directions.push_back(Direction::Bottom); }
    if (pos.x < (size.width-1)) { directions.push_back(Direction::East); }
    if (pos.y < (size.depth-1)) { directions.push_back(Direction::North); }
    if (pos.z < (size.height-1)) { directions.push_back(Direction::Top); }

    return directions;

}
