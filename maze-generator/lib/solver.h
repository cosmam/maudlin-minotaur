#ifndef SOLVER_H
#define SOLVER_H

#include <vector>

#include "basetypes.h"
#include "constraints.h"
#include "mazebuilder.h"

namespace Solver {

struct Solution {
    MazeBuilder builder;
    Constraints constraints;
    std::vector<Path> paths;
};

Path step(const MazeBuilder & builder, const Constraints & constraints, const Path & path, direction_shuffle_func func);

Path findPath(const MazeBuilder & builder, const Constraints & constraints, Position start, direction_shuffle_func func);

std::vector<Step> getSteps(const Path & path);

MazeBuilder stepPath(MazeBuilder builder, Step step);

MazeBuilder walkPath(MazeBuilder builder, const Path & path);

Path findConstrainedPath(MazeBuilder builder, const Constraints & constraints, std::pair<Position, Position> & connection, direction_shuffle_func dir_func);

Solution solveRequiredConnections(MazeBuilder builder, const Constraints & constraints, direction_shuffle_func dir_func, random_uint_func uint_func);

MazeBuilder eraseWalls(MazeBuilder builder, const Constraints & constraints);

Solution solve(const Maze & maze, const Constraints & constraints, direction_shuffle_func dir_func, random_uint_func uint_func);

Solution solve(const Maze & maze, const Constraints & constraints);

}

#endif // SOLVER_H
