#include "solutionstepper.h"

using Solver::Solution;

SolutionStepper::SolutionStepper()
    : _path_index(0),
      _step_index(0),
      _state(SteppingState::Initial)
{}

std::vector<MazeRoom> SolutionStepper::setSolution(Solution solution) {
    _solution = solution;
    _builder = MazeBuilder(Maze{solution.builder.getMaze().size()});
    return reset();
}

std::vector<MazeRoom> SolutionStepper::reset() {
    if(!_solution.has_value()) {
        return std::vector<MazeRoom>();
    }

    _rooms.clear();
    _path_index = 0;
    _step_index = 0;
    _state = SteppingState::Initial;

    for(auto position : generatePositions(_builder->getMaze().size())) {
        _rooms[position] = MazeRoom{position, AllDirections, RoomState::Unvisited};
    }

    return getRooms();
}

std::vector<MazeRoom> SolutionStepper::step() {
    std::vector<MazeRoom> rooms;

    switch(_state) {
    case SteppingState::Initial:
        rooms = step_initial();
        break;
    case SteppingState::Start_Of_Find_Path:
        rooms = start_find_path();
        break;
    case SteppingState::Stepping_Find:
        rooms = step_find_single();
        break;
    case SteppingState::End_Of_Find_Path:
        rooms = step_find_path();
        break;
    case SteppingState::Stepping_Visit:
        rooms = step_visit_single();
        break;
    case SteppingState::End_Of_Visit_Path:
        rooms = step_visit_path();
        break;
    case SteppingState::Finished:
        rooms = std::vector<MazeRoom>();
        break;
    }

    return rooms;
}

// visit initial target square, which is pointed to by the end of the first path
std::vector<MazeRoom> SolutionStepper::step_initial() {
    auto step = _solution->paths.front().steps.back();
    auto pos = calculatePosition(step.position, step.direction);
    _builder->visit(pos, AllDirections);
    _rooms[pos].state = RoomState::Visited;

    _state = SteppingState::Start_Of_Find_Path;
    return std::vector<MazeRoom>{_rooms[pos]};
}

std::vector<MazeRoom> SolutionStepper::start_find_path()
{
    std::vector<MazeRoom> rooms;
    auto step = _solution->paths.at(_path_index).steps.at(_step_index);

    for(auto it = _rooms.begin() ; it != _rooms.end() ; ++it) {
        if(it->second.state == RoomState::OnPath) {
            it->second.state = RoomState::Unvisited;
            rooms.push_back(it->second);
        }
    }
    _rooms[step.position].state = RoomState::Current;
    rooms.push_back(_rooms[step.position]);

    _state = SteppingState::Stepping_Find;

    return rooms;
}

std::vector<MazeRoom> SolutionStepper::step_find_single() {
    std::vector<MazeRoom> rooms;
    auto step = _solution->paths.at(_path_index).steps.at(_step_index);
    auto nextPos = calculatePosition(step.position, step.direction);

    _rooms[step.position].state = RoomState::OnPath;
    _rooms[nextPos].state = RoomState::Current;

    rooms.push_back(_rooms[step.position]);
    rooms.push_back(_rooms[nextPos]);

    ++_step_index;

    _state = (_step_index == _solution->paths.at(_path_index).steps.size() ? SteppingState::End_Of_Find_Path : SteppingState::Stepping_Find);
    return rooms;
}

std::vector<MazeRoom> SolutionStepper::step_visit_single() {
    auto step = _steps.at(_step_index);
    auto nextPos = calculatePosition(step.position, step.direction);

    _builder = Solver::stepPath(*_builder, step);
    _rooms[step.position].walls = _builder->getMaze().walls(step.position);
    _rooms[step.position].state = RoomState::Visited;
    _rooms[nextPos].walls = _builder->getMaze().walls(nextPos);
    _rooms[nextPos].state = RoomState::Current;

    ++_step_index;

    _state = (_step_index == _steps.size() ? SteppingState::End_Of_Visit_Path : SteppingState::Stepping_Visit);
    return std::vector<MazeRoom>{_rooms[step.position], _rooms[nextPos]};
}

std::vector<MazeRoom> SolutionStepper::step_find_path() {
    _steps = Solver::getSteps(_solution->paths[_path_index]);
    auto pos = calculatePosition(_steps.back().position, _steps.back().direction);
    _rooms[pos].state = RoomState::Visited;

    _step_index = 0;

    auto rooms = step_visit_single();
    rooms.push_back(_rooms[pos]);
    return rooms;
}

std::vector<MazeRoom> SolutionStepper::step_visit_path() {
    auto pos = calculatePosition(_steps.back().position, _steps.back().direction);
    _rooms[pos].state = RoomState::Visited;

    _steps.clear();
    ++_path_index;

    if(_path_index >= _solution->paths.size()) {
        _state = SteppingState::Finished;
        return std::vector<MazeRoom>{_rooms[pos]};
    } else {
        _step_index = 0;
        auto rooms = start_find_path();
        rooms.push_back(_rooms[pos]);
        return rooms;
    }
}

std::vector<MazeRoom> SolutionStepper::finish() {
    _builder = _solution->builder;
    _steps.clear();
    _rooms.clear();
    _path_index = _solution->paths.size();
    _step_index = _solution->paths.back().steps.size();
    _state = SteppingState::Finished;

    for(auto position : generatePositions(_builder->getMaze().size())) {
        _rooms[position] = MazeRoom{position, _builder->getMaze().walls(position), RoomState::Visited};
    }

    return getRooms();
}

std::vector<MazeRoom> SolutionStepper::getRooms() const {
    std::vector<MazeRoom> v;
    for( auto it = _rooms.begin(); it != _rooms.end(); ++it ) {
        v.push_back( it->second );
    }
    return v;
}
