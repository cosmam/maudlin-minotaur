#include "maze.h"

#include <algorithm>
#include <assert.h>

Maze::Maze(Size size)
    : _walls(size, true)
{}

bool Maze::isWall(Position position, Direction wall) const {
    return _walls.getData(position, wall);
}

void Maze::setWall(Position position, Direction wall, bool exists) {
    _walls.setData(position, wall, exists);
}

std::unordered_set<Direction> Maze::walls(Position position) const {
    std::unordered_set<Direction> walls;
    for(auto direction : AllDirections) {
        if (this->isWall(position, direction)) {
            walls.insert(direction);
        }
    }

    return walls;
}

