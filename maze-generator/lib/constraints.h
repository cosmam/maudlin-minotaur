#ifndef CONSTRAINTS_H
#define CONSTRAINTS_H

#include <vector>

#include "basetypes.h"
#include "walls.h"

class Constraints {
public:
    Constraints(Size size);

    Size size() const { return _walls.size(); }

    WallConstraint getConstraint(Position position, Direction wall) const;

    void setConstraint(Position position, Direction wall, WallConstraint constraint);

    std::vector<std::pair<Position, Position>> getConnections() const;

    void setConnections(std::vector<std::pair<Position, Position>> connections);

private:

    Walls<WallConstraint> _walls;
    std::vector<std::pair<Position, Position>> _connections;
};

#endif // CONSTRAINTS_H
