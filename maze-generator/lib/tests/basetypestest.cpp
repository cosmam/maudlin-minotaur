#include "basetypestest.h"
using namespace Test;

#include "lib/basetypes.h"

Q_DECLARE_METATYPE(Direction)

BaseTypesTest::BaseTypesTest(QObject *parent) :
    QObject(parent)
{
}

void BaseTypesTest::initTestCase()
{

}

void BaseTypesTest::cleanupTestCase()
{

}

void BaseTypesTest::init()
{
}

void BaseTypesTest::cleanup()
{
}

void BaseTypesTest::TestPositionEquality_data()
{
    QTest::addColumn<unsigned int>("x_left");
    QTest::addColumn<unsigned int>("y_left");
    QTest::addColumn<unsigned int>("z_left");
    QTest::addColumn<unsigned int>("x_right");
    QTest::addColumn<unsigned int>("y_right");
    QTest::addColumn<unsigned int>("z_right");
    QTest::addColumn<bool>("equals");

    unsigned int x = 1;
    unsigned int y = 1;
    unsigned int z = 1;

    QTest::newRow("equal") << x << x << y << y << z << z << true;
    QTest::newRow("x") << x << ++x << y << y << z << z << false;
    QTest::newRow("y") << x << x << y << ++y << z << z << false;
    QTest::newRow("z") << x << x << y << y << z << ++z << false;
    QTest::newRow("xy") << x << ++x << y << ++y << z << z << false;
    QTest::newRow("xz") << x << ++x << y << y << z << ++z << false;
    QTest::newRow("yz") << x << x << y << ++y << z << ++z << false;
    QTest::newRow("xyz") << x << ++x << y << ++y << z << ++z << false;
}

void BaseTypesTest::TestPositionEquality()
{
    QFETCH(unsigned int, x_left);
    QFETCH(unsigned int, y_left);
    QFETCH(unsigned int, z_left);
    QFETCH(unsigned int, x_right);
    QFETCH(unsigned int, y_right);
    QFETCH(unsigned int, z_right);
    QFETCH(bool, equals);
    Position lhs{x_left, y_left, z_left};
    Position rhs{x_right, y_right, z_right};

    QCOMPARE(lhs == rhs, equals);

}

void BaseTypesTest::TestCalculatePosition_data()
{
    QTest::addColumn<unsigned int>("x_left");
    QTest::addColumn<unsigned int>("y_left");
    QTest::addColumn<unsigned int>("z_left");
    QTest::addColumn<Direction>("direction");
    QTest::addColumn<unsigned int>("x_right");
    QTest::addColumn<unsigned int>("y_right");
    QTest::addColumn<unsigned int>("z_right");

    unsigned int x = 2;
    unsigned int y = 4;
    unsigned int z = 6;

    QTest::newRow("N") << x << y << z << Direction::North << x << ++y << z;
    QTest::newRow("N") << x << y << z << Direction::East << ++x << y << z;
    QTest::newRow("N") << x << y << z << Direction::South << x << --y << z;
    QTest::newRow("N") << x << y << z << Direction::West << --x << y << z;
    QTest::newRow("N") << x << y << z << Direction::Top << x << y << ++z;
    QTest::newRow("N") << x << y << z << Direction::Bottom << x << y << --z;
}

void BaseTypesTest::TestCalculatePosition()
{
    QFETCH(unsigned int, x_left);
    QFETCH(unsigned int, y_left);
    QFETCH(unsigned int, z_left);
    QFETCH(Direction, direction);
    QFETCH(unsigned int, x_right);
    QFETCH(unsigned int, y_right);
    QFETCH(unsigned int, z_right);
    Position lhs{x_left, y_left, z_left};
    Position rhs{x_right, y_right, z_right};

    QCOMPARE(calculatePosition(lhs, direction), rhs);
}
