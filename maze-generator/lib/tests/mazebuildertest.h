#ifndef MAZEBUILDERTEST_H
#define MAZEBUILDERTEST_H

#include <QObject>
#include <QPointer>
#include <QtTest>

#include "lib/basetypes.h"

namespace Test {

    class MazeBuilderTest : public QObject
    {
            Q_OBJECT

        public:
            explicit MazeBuilderTest(QObject *parent = 0);

        signals:

        public slots:

        private:

        private Q_SLOTS:

            void initTestCase();
            void cleanupTestCase();
            void init();
            void cleanup();

            void TestMaze_data();
            void TestMaze();
            void TestVisit_data();
            void TestVisit();
            void TestIsFullyVisited_data();
            void TestIsFullyVisited();
            void TestRandomUnvisited_data();
            void TestRandomUnvisited();
            void TestDirections_data();
            void TestDirections();
            void TestSetWall_data();
            void TestSetWall();
    };
}

#endif // MAZEBUILDERTEST_H
