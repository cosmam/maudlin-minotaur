#include "solvertest.h"

#include <queue>

#include "lib/constraints.h"
#include "lib/mazebuilder.h"
#include "lib/solver.h"

using namespace Test;

namespace {

    const auto DirList = std::vector<Direction>{Direction::North, Direction::South, Direction::East, Direction::West, Direction::Top, Direction::Bottom};

    std::vector<Direction> _directions;
    std::queue<std::vector<Direction>> _direction_queue;

    std::vector<Direction> fake_direction_shuffle(std::vector<Direction> directions) {
        std::vector<Direction> shuffled;
        for(auto direction : DirList) {
            if(std::find(directions.begin(), directions.end(), direction) != directions.end()) {
                shuffled.push_back(direction);
            }
        }
        return shuffled;
    }

    std::vector<Direction> fake_queue_direction_shuffle(std::vector<Direction> directions) {
        auto reference = _direction_queue.front();
        _direction_queue.pop();
        std::vector<Direction> shuffled;
        for(auto direction : reference) {
            if(std::find(directions.begin(), directions.end(), direction) != directions.end()) {
                shuffled.push_back(direction);
            }
        }
        return shuffled;
    }

    std::vector<Position> getPositions(unsigned int width, unsigned int depth, unsigned int height) {
        std::vector<Position> positions;
        for(unsigned int i=0 ; i < width ; ++i) {
            for(unsigned int j=0 ; j < depth ; ++j) {
                for(unsigned int k=0 ; k < height ; ++k) {
                    positions.push_back(Position{i, j, k});
                }
            }
        }
        return positions;
    }
}

SolverTest::SolverTest(QObject *parent) :
    QObject(parent)
{
}

void SolverTest::initTestCase()
{
    for(auto direction : AllDirections) {
        _directions.push_back(direction);
    }
}

void SolverTest::cleanupTestCase()
{
}

void SolverTest::init()
{
}

void SolverTest::cleanup()
{
}

void SolverTest::TestStep_NoConstraints_StartingPath_NotFinished()
{
    MazeBuilder builder{Maze(Size{1, 3, 1})};
    Constraints constraints(Size{1, 3, 1});

    auto oldPos = Position{0,0,0};
    Path path{oldPos, std::vector<Step>(), false};

    auto newPath = Solver::step(builder, constraints, path, fake_direction_shuffle);
    auto newPos = Position{0,1,0};

    QCOMPARE(newPos, newPath.current);
    QCOMPARE(newPath.steps.size(), (unsigned int)1);
    QCOMPARE(newPath.steps.back().position, oldPos);
    QCOMPARE(newPath.steps.back().direction, Direction::North);
    QCOMPARE(newPath.isFinished, false);
}

void SolverTest::TestStep_NoConstraints_StartingPath_Finished()
{
    MazeBuilder builder{Maze(Size{1, 3, 1})};
    Constraints constraints(Size{1, 3, 1});
    builder.visit(Position{0,1,0}, AllDirections);

    auto oldPos = Position{0,0,0};
    Path path{oldPos, std::vector<Step>(), false};

    auto newPath = Solver::step(builder, constraints, path, fake_direction_shuffle);
    auto newPos = Position{0,1,0};

    QCOMPARE(newPos, newPath.current);
    QCOMPARE(newPath.steps.size(), (unsigned int)1);
    QCOMPARE(newPath.steps.back().position, oldPos);
    QCOMPARE(newPath.steps.back().direction, Direction::North);
    QCOMPARE(newPath.isFinished, true);
}

void SolverTest::TestStep_NoConstraints_ContinuePath_NotFinished()
{
    MazeBuilder builder{Maze(Size{1, 4, 1})};
    Constraints constraints(Size{1, 4, 1});
    auto steps = std::vector<Step>(1, Step{Position{0,0,0}, Direction::North});

    auto oldPos = Position{0,1,0};
    Path path{oldPos, steps, false};

    auto newPath = Solver::step(builder, constraints, path, fake_direction_shuffle);
    auto newPos = Position{0,2,0};

    QCOMPARE(newPos, newPath.current);
    QCOMPARE(newPath.steps.size(), (unsigned int)2);
    QCOMPARE(newPath.steps.back().position, oldPos);
    QCOMPARE(newPath.steps.back().direction, Direction::North);
    QCOMPARE(newPath.isFinished, false);
}

void SolverTest::TestStep_NoConstraints_ContinuePath_Finished()
{
    MazeBuilder builder{Maze(Size{1, 4, 1})};
    Constraints constraints(Size{1, 4, 1});
    auto steps = std::vector<Step>(1, Step{Position{0,0,0}, Direction::North});
    builder.visit(Position{0,2,0}, AllDirections);

    auto oldPos = Position{0,1,0};
    Path path{oldPos, steps, false};

    auto newPath = Solver::step(builder, constraints, path, fake_direction_shuffle);
    auto newPos = Position{0,2,0};

    QCOMPARE(newPos, newPath.current);
    QCOMPARE(newPath.steps.size(), (unsigned int)2);
    QCOMPARE(newPath.steps.back().position, oldPos);
    QCOMPARE(newPath.steps.back().direction, Direction::North);
    QCOMPARE(newPath.isFinished, true);
}
void SolverTest::TestStep_Constraints_StartingPath_NotFinished()
{
    MazeBuilder builder{Maze(Size{2, 2, 1})};
    Constraints constraints(Size{2, 2, 1});

    auto oldPos = Position{0,0,0};
    constraints.setConstraints(oldPos, Direction::North, WallConstraint::Exists);
    Path path{oldPos, std::vector<Step>(), false};

    auto newPath = Solver::step(builder, constraints, path, fake_direction_shuffle);
    auto newPos = Position{1,0,0};

    QCOMPARE(newPos, newPath.current);
    QCOMPARE(newPath.steps.size(), (unsigned int)1);
    QCOMPARE(newPath.steps.back().position, oldPos);
    QCOMPARE(newPath.steps.back().direction, Direction::East);
    QCOMPARE(newPath.isFinished, false);
}

void SolverTest::TestStep_Constraints_StartingPath_Finished()
{
    MazeBuilder builder{Maze(Size{2, 2, 1})};
    Constraints constraints(Size{2, 2, 1});
    builder.visit(Position{1,0,0}, AllDirections);

    auto oldPos = Position{0,0,0};
    constraints.setConstraints(oldPos, Direction::North, WallConstraint::Exists);
    Path path{oldPos, std::vector<Step>(), false};

    auto newPath = Solver::step(builder, constraints, path, fake_direction_shuffle);
    auto newPos = Position{1,0,0};

    QCOMPARE(newPos, newPath.current);
    QCOMPARE(newPath.steps.size(), (unsigned int)1);
    QCOMPARE(newPath.steps.back().position, oldPos);
    QCOMPARE(newPath.steps.back().direction, Direction::East);
    QCOMPARE(newPath.isFinished, true);
}

void SolverTest::TestStep_Constraints_ContinuePath_NotFinished()
{
    MazeBuilder builder{Maze(Size{3, 2, 1})};
    Constraints constraints(Size{3, 2, 1});
    auto steps = std::vector<Step>(1, Step{Position{0,0,0}, Direction::East});

    auto oldPos = Position{1,0,0};
    constraints.setConstraints(oldPos, Direction::North, WallConstraint::Exists);
    Path path{oldPos, steps, false};

    auto newPath = Solver::step(builder, constraints, path, fake_direction_shuffle);
    auto newPos = Position{2,0,0};

    QCOMPARE(newPos, newPath.current);
    QCOMPARE(newPath.steps.size(), (unsigned int)2);
    QCOMPARE(newPath.steps.back().position, oldPos);
    QCOMPARE(newPath.steps.back().direction, Direction::East);
    QCOMPARE(newPath.isFinished, false);
}

void SolverTest::TestStep_Constraints_ContinuePath_Finished()
{
    MazeBuilder builder{Maze(Size{3, 2, 1})};
    Constraints constraints(Size{3, 2, 1});
    auto steps = std::vector<Step>(1, Step{Position{0,0,0}, Direction::East});
    builder.visit(Position{2,0,0}, AllDirections);

    auto oldPos = Position{1,0,0};
    constraints.setConstraints(oldPos, Direction::North, WallConstraint::Exists);
    Path path{oldPos, steps, false};

    auto newPath = Solver::step(builder, constraints, path, fake_direction_shuffle);
    auto newPos = Position{2,0,0};

    QCOMPARE(newPos, newPath.current);
    QCOMPARE(newPath.steps.size(), (unsigned int)2);
    QCOMPARE(newPath.steps.back().position, oldPos);
    QCOMPARE(newPath.steps.back().direction, Direction::East);
    QCOMPARE(newPath.isFinished, true);
}
void SolverTest::TestFind_Path_NoConstraints()
{
    MazeBuilder builder{Maze(Size{2, 2, 1})};
    Constraints constraints(Size{2, 2, 1});
    builder.visit(Position{1, 1, 0}, AllDirections);

    _direction_queue = std::queue<std::vector<Direction>>();
    _direction_queue.push(std::vector<Direction>{Direction::North, Direction::South, Direction::East, Direction::West, Direction::Top, Direction::Bottom});
    _direction_queue.push(std::vector<Direction>{Direction::East, Direction::West, Direction::North, Direction::South, Direction::Top, Direction::Bottom});

    auto expected = Path{Position{1, 1, 0}, std::vector<Step>{Step{Position{0, 0, 0}, Direction::North}, Step{Position{0, 1, 0}, Direction::East}}, true};
    auto path = Solver::findPath(builder, constraints, Position{0, 0, 0}, fake_queue_direction_shuffle);

    QCOMPARE(path.current, expected.current);
    QCOMPARE(path.steps.size(), expected.steps.size());
    QVERIFY(path.isFinished);
    for(unsigned int i=0 ; i < path.steps.size() ; ++i) {
        QCOMPARE(path.steps.at(i).position, expected.steps.at(i).position);
        QCOMPARE(path.steps.at(i).direction, expected.steps.at(i).direction);
    }
}

void SolverTest::TestFind_Path_Constraints()
{
    MazeBuilder builder{Maze(Size{2, 2, 1})};
    Constraints constraints(Size{2, 2, 1});
    constraints.setConstraints(Position{0, 0, 0}, Direction::North, WallConstraint::Exists);
    builder.visit(Position{1, 1, 0}, AllDirections);

    _direction_queue = std::queue<std::vector<Direction>>();
    _direction_queue.push(std::vector<Direction>{Direction::North, Direction::South, Direction::East, Direction::West, Direction::Top, Direction::Bottom});
    _direction_queue.push(std::vector<Direction>{Direction::North, Direction::South, Direction::East, Direction::West, Direction::Top, Direction::Bottom});

    auto expected = Path{Position{1, 1, 0}, std::vector<Step>{Step{Position{0, 0, 0}, Direction::East}, Step{Position{1, 0, 0}, Direction::North}}, true};
    auto path = Solver::findPath(builder, constraints, Position{0, 0, 0}, fake_queue_direction_shuffle);

    QCOMPARE(path.current, expected.current);
    QCOMPARE(path.steps.size(), expected.steps.size());
    QVERIFY(path.isFinished);
    for(unsigned int i=0 ; i < path.steps.size() ; ++i) {
        QCOMPARE(path.steps.at(i).position, expected.steps.at(i).position);
        QCOMPARE(path.steps.at(i).direction, expected.steps.at(i).direction);
    }
}

void SolverTest::TestGetSteps_One()
{
    auto path = Path{Position{2, 0, 0},
                     std::vector<Step>{Step{Position{0,0,0}, Direction::North},
                                       Step{Position{0,1,0}, Direction::North},
                                       Step{Position{0,2,0}, Direction::East},
                                       Step{Position{2,1,0}, Direction::South},
                                       Step{Position{1,1,0}, Direction::West},
                                       Step{Position{0,1,0}, Direction::South},
                                       Step{Position{0,0,0}, Direction::East},
                                       Step{Position{1,0,0}, Direction::East}},
                     true};
    auto expected = std::vector<Step>{Step{Position{0,0,0}, Direction::East},
                                      Step{Position{1,0,0}, Direction::East}};

    auto actual = Solver::getSteps(path);

    QCOMPARE(actual.size(), expected.size());
    for(unsigned int i=0 ; i < actual.size() ; ++i) {
        QCOMPARE(actual.at(i).position, expected.at(i).position);
        QCOMPARE(actual.at(i).direction, expected.at(i).direction);
    }
}

void SolverTest::TestGetSteps_Two()
{
    auto path = Path{Position{2, 0, 0},
                     std::vector<Step>{Step{Position{0,0,0}, Direction::North},
                                       Step{Position{0,1,0}, Direction::North},
                                       Step{Position{0,2,0}, Direction::East},
                                       Step{Position{1,2,0}, Direction::South},
                                       Step{Position{1,1,0}, Direction::West},
                                       Step{Position{0,1,0}, Direction::East},
                                       Step{Position{1,1,0}, Direction::East},
                                       Step{Position{2,1,0}, Direction::South}},
                     true};
    auto expected = std::vector<Step>{Step{Position{0,0,0}, Direction::North},
                                      Step{Position{0,1,0}, Direction::East},
                                      Step{Position{1,1,0}, Direction::East},
                                      Step{Position{2,1,0}, Direction::South}};

    auto actual = Solver::getSteps(path);

    QCOMPARE(actual.size(), expected.size());
    for(unsigned int i=0 ; i < actual.size() ; ++i) {
        QCOMPARE(actual.at(i).position, expected.at(i).position);
        QCOMPARE(actual.at(i).direction, expected.at(i).direction);
    }
}

void SolverTest::TestWalkPath_Fresh()
{
    MazeBuilder builder{Maze(Size{3, 3, 3})};
    auto path = Path{Position{1,2,0},
                     std::vector<Step>{Step{Position{0,0,0}, Direction::North},
                                       Step{Position{0,1,0}, Direction::East},
                                       Step{Position{1,1,0}, Direction::North}},
                     true};

    MazeBuilder expected{Maze(Size{3, 3, 3})};
    expected.visit(Position{0,0,0}, std::unordered_set<Direction>{Direction::South, Direction::East, Direction::West, Direction::Top, Direction::Bottom});
    expected.visit(Position{0,1,0}, std::unordered_set<Direction>{Direction::North, Direction::West, Direction::Top, Direction::Bottom});
    expected.visit(Position{1,1,0}, std::unordered_set<Direction>{Direction::South, Direction::East, Direction::Top, Direction::Bottom});

    auto actual = Solver::walkPath(builder, path);

    for(auto position : getPositions(3, 3, 3)) {
        QCOMPARE(actual.isVisited(position), expected.isVisited(position));
        QCOMPARE(actual.getMaze().walls(position), expected.getMaze().walls(position));
    }
}

void SolverTest::TestWalkPath_PartiallySolved()
{
    MazeBuilder builder{Maze(Size{3, 3, 3})};
    builder.visit(Position{0,2,0}, std::unordered_set<Direction>{Direction::North, Direction::South, Direction::West, Direction::Top, Direction::Bottom});
    builder.visit(Position{1,2,0}, std::unordered_set<Direction>{Direction::North, Direction::East, Direction::Top, Direction::Bottom});
    builder.visit(Position{1,1,0}, std::unordered_set<Direction>{Direction::East, Direction::West, Direction::Top, Direction::Bottom});

    auto path = Path{Position{2,1,0},
                     std::vector<Step>{Step{Position{0,0,0}, Direction::East},
                                       Step{Position{1,0,0}, Direction::East},
                                       Step{Position{2,0,0}, Direction::North}},
                     true};

    MazeBuilder expected = builder;
    expected.visit(Position{0,0,0}, std::unordered_set<Direction>{Direction::North, Direction::South, Direction::West, Direction::Top, Direction::Bottom});
    expected.visit(Position{1,0,0}, std::unordered_set<Direction>{Direction::South, Direction::Top, Direction::Bottom});
    expected.visit(Position{2,0,0}, std::unordered_set<Direction>{Direction::South, Direction::East, Direction::Top, Direction::Bottom});

    auto actual = Solver::walkPath(builder, path);

    for(auto position : getPositions(3, 3, 3)) {
        QCOMPARE(actual.isVisited(position), expected.isVisited(position));
        QCOMPARE(actual.getMaze().walls(position), expected.getMaze().walls(position));
    }
}
