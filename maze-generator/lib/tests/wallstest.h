#ifndef WALLSTEST_H
#define WALLSTEST_H

#include <QObject>
#include <QPointer>
#include <QtTest>

namespace Test {

    class WallsTest : public QObject
    {
            Q_OBJECT

        public:
            explicit WallsTest(QObject *parent = 0);

        signals:

        public slots:

        private:

        private Q_SLOTS:

            void initTestCase();
            void cleanupTestCase();
            void init();
            void cleanup();

            void TestSize_data();
            void TestSize();
            void TestData_data();
            void TestData();
            void TestComplexIndices_data();
            void TestComplexIndices();
    };

}

#endif // WALLSTEST_H
