#ifndef SOLVERTEST_H
#define SOLVERTEST_H

#include <QObject>
#include <QPointer>
#include <QtTest>

#include "lib/basetypes.h"

namespace Test {

    class SolverTest : public QObject
    {
            Q_OBJECT

        public:
            explicit SolverTest(QObject *parent = 0);

        signals:

        public slots:

        private:

        private Q_SLOTS:

            void initTestCase();
            void cleanupTestCase();
            void init();
            void cleanup();

            void TestStep_NoConstraints_StartingPath_NotFinished();
            void TestStep_NoConstraints_StartingPath_Finished();
            void TestStep_NoConstraints_ContinuePath_NotFinished();
            void TestStep_NoConstraints_ContinuePath_Finished();
            void TestStep_Constraints_StartingPath_NotFinished();
            void TestStep_Constraints_StartingPath_Finished();
            void TestStep_Constraints_ContinuePath_NotFinished();
            void TestStep_Constraints_ContinuePath_Finished();
            void TestFind_Path_NoConstraints();
            void TestFind_Path_Constraints();
            void TestGetSteps_One();
            void TestGetSteps_Two();
            void TestWalkPath_Fresh();
            void TestWalkPath_PartiallySolved();
    };
}

#endif // SOLVERTEST_H
