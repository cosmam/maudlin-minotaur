#ifndef MAZETEST_H
#define MAZETEST_H

#include <QObject>
#include <QPointer>
#include <QtTest>

namespace Test {

    class MazeTest : public QObject
    {
            Q_OBJECT

        public:
            explicit MazeTest(QObject *parent = 0);

        signals:

        public slots:

        private:

        private Q_SLOTS:

            void initTestCase();
            void cleanupTestCase();
            void init();
            void cleanup();

            void TestSize_data();
            void TestSize();
            void TestWall_data();
            void TestWall();
            void TestWalls_data();
            void TestWalls();
    };
}

#endif // MAZETEST_H
