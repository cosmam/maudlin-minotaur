#include "mazebuildertest.h"

#include <queue>
#include <unordered_set>

#include <QVector>

#include "lib/mazebuilder.h"

#include "unit_test/testchasis.h"

Q_DECLARE_METATYPE(Direction)
Q_DECLARE_METATYPE(Position)

using namespace Test;

namespace {

    const std::vector<Direction> DIRECTION_VECTOR{AllDirections.begin(), AllDirections.end()};

    std::queue<unsigned int> _indexes;

    unsigned int fake_random_uint(unsigned int, unsigned int) {
        auto value = _indexes.front();
        if(_indexes.size() > 1) { _indexes.pop(); }
        return value;
    }
}

MazeBuilderTest::MazeBuilderTest(QObject *parent) :
    QObject(parent)
{
}

void MazeBuilderTest::initTestCase()
{
}

void MazeBuilderTest::cleanupTestCase()
{
}

void MazeBuilderTest::init()
{
}

void MazeBuilderTest::cleanup()
{
}

void MazeBuilderTest::TestMaze_data()
{
    QTest::addColumn<unsigned int>("width");
    QTest::addColumn<unsigned int>("depth");
    QTest::addColumn<unsigned int>("height");

    for(int i=0 ; i<10 ; ++i) {
        QTest::newRow(qPrintable(QString::number(i))) << (unsigned int)TestChasis::RandInt(0, 10000) << (unsigned int)TestChasis::RandInt(0, 10000) << (unsigned int)TestChasis::RandInt(0, 10000);
    }
}

void MazeBuilderTest::TestMaze()
{
    QFETCH(unsigned int, width);
    QFETCH(unsigned int, depth);
    QFETCH(unsigned int, height);
    Size size{width, depth, height};
    Maze maze(size);
    MazeBuilder builder(maze);

    QCOMPARE(builder.getMaze().size(), maze.size());
}

void MazeBuilderTest::TestVisit_data()
{
    QTest::addColumn<unsigned int>("x");
    QTest::addColumn<unsigned int>("y");
    QTest::addColumn<unsigned int>("z");
    QTest::addColumn<QVector<Direction>>("directions");

    unsigned int x = 1;
    unsigned int y = 1;
    unsigned int z = 3;

    QTest::newRow("all") << x-1 << y << z << QVector<Direction>{Direction::North, Direction::South, Direction::East, Direction::West, Direction::Top, Direction::Bottom};
    QTest::newRow("none") << x << y+1 << z << QVector<Direction>();
    QTest::newRow("partial_1") << x << y << z << QVector<Direction>{Direction::West, Direction::Top, Direction::Bottom};
    QTest::newRow("partial_2") << x-1 << y << z+1 << QVector<Direction>{Direction::North, Direction::West, Direction::Top, Direction::Bottom};
    QTest::newRow("partial_3") << x << y << z << QVector<Direction>{Direction::North, Direction::West, Direction::Top};
}

void MazeBuilderTest::TestVisit()
{
    QFETCH(unsigned int, x);
    QFETCH(unsigned int, y);
    QFETCH(unsigned int, z);
    QFETCH(QVector<Direction>, directions);

    Position pos{x, y, z};
    std::unordered_set<Direction> walls;
    for(auto direction : directions) {
        walls.insert(direction);
    }

    MazeBuilder builder(Maze(Size{2, 3, 5}));
    builder.visit(pos, walls);
    QVERIFY(builder.isVisited(pos));
    for(auto direction : AllDirections) {
        QCOMPARE(builder.getMaze().isWall(pos, direction), directions.contains(direction));
    }
}

void MazeBuilderTest::TestIsFullyVisited_data()
{
    QTest::addColumn<unsigned int>("width");
    QTest::addColumn<unsigned int>("depth");
    QTest::addColumn<unsigned int>("height");
    QTest::addColumn<unsigned int>("x");
    QTest::addColumn<unsigned int>("y");
    QTest::addColumn<unsigned int>("z");

    unsigned int zero = 0;
    unsigned int one = 1;
    unsigned int width = 2;
    unsigned int depth = 3;
    unsigned int height = 5;

    QTest::newRow("large") << width << depth << height << width-1 << depth-1 << height-1;
    QTest::newRow("minimal") << one << one << one << zero << zero << zero;
}

void MazeBuilderTest::TestIsFullyVisited()
{
    QFETCH(unsigned int, width);
    QFETCH(unsigned int, depth);
    QFETCH(unsigned int, height);
    QFETCH(unsigned int, x);
    QFETCH(unsigned int, y);
    QFETCH(unsigned int, z);

    MazeBuilder builder(Maze(Size{width, depth, height}));
    Position unvisited{x, y, z};

    QCOMPARE(builder.isFullyVisited(), false);

    for(unsigned int i=0 ; i<width ; ++i) {
        for(unsigned int j=0 ; j<depth ; ++j) {
            for(unsigned int k=0 ; k<height ; ++k) {
                Position pos{i, j, k};
                if(pos != unvisited) {
                    builder.visit(Position{i, j, k}, AllDirections);
                }
            }
        }
    }

    QCOMPARE(builder.isFullyVisited(), false);
    builder.visit(unvisited, AllDirections);
    QVERIFY(builder.isFullyVisited());
}

void MazeBuilderTest::TestRandomUnvisited_data()
{
    QTest::addColumn<int>("count");

    for(int i=0 ; i < 10 ; ++i) {
        QTest::newRow(qPrintable(QString::number(i))) << TestChasis::RandInt(1, 30);
    }
    QTest::newRow("all") << 30;
}

void MazeBuilderTest::TestRandomUnvisited()
{
    QFETCH(int, count);

    MazeBuilder builder(Maze(Size{2, 3, 5}));

    _indexes = std::queue<unsigned int>();
    for(unsigned int i=0 ; i < (unsigned int)count ; ++i) {
        _indexes.push(i);
        _indexes.push(i);
    }

    for(int i=0 ; i < count ; ++i) {
        auto pos = builder.randomUnivisited(fake_random_uint);
        QCOMPARE(builder.isVisited(pos), false);
        builder.visit(pos, AllDirections);
    }
}

void MazeBuilderTest::TestDirections_data()
{
    QTest::addColumn<unsigned int>("x");
    QTest::addColumn<unsigned int>("y");
    QTest::addColumn<unsigned int>("z");
    QTest::addColumn<QVector<Direction>>("directions");

    unsigned int low = 0;
    unsigned int mid = 1;
    unsigned int high = 2;

    QTest::newRow("all") << mid << mid << mid << QVector<Direction>{Direction::North, Direction::South, Direction::East, Direction::West, Direction::Top, Direction::Bottom};
    QTest::newRow("swb corner") << low << low << low << QVector<Direction>{Direction::North, Direction::East, Direction::Top};
    QTest::newRow("nwb corner") << low << high << low << QVector<Direction>{Direction::South, Direction::East, Direction::Top};
    QTest::newRow("neb corner") << high << high << low << QVector<Direction>{Direction::South, Direction::West, Direction::Top};
    QTest::newRow("seb corner") << high << low << low << QVector<Direction>{Direction::North, Direction::West, Direction::Top};
    QTest::newRow("swt corner") << low << low << high << QVector<Direction>{Direction::North, Direction::East, Direction::Bottom};
    QTest::newRow("nwt corner") << low << high << high << QVector<Direction>{Direction::South, Direction::East, Direction::Bottom};
    QTest::newRow("net corner") << high << high << high << QVector<Direction>{Direction::South, Direction::West, Direction::Bottom};
    QTest::newRow("set corner") << high << low << high << QVector<Direction>{Direction::North, Direction::West, Direction::Bottom};
    QTest::newRow("wb edge") << low << mid << low << QVector<Direction>{Direction::North, Direction::South, Direction::East, Direction::Top};
    QTest::newRow("nb edge") << mid << high << low << QVector<Direction>{Direction::South, Direction::East, Direction::West, Direction::Top};
    QTest::newRow("eb edge") << high << mid << low << QVector<Direction>{Direction::North, Direction::South, Direction::West, Direction::Top};
    QTest::newRow("sb edge") << mid << low << low << QVector<Direction>{Direction::North, Direction::East, Direction::West, Direction::Top};
    QTest::newRow("sw edge") << low << low << mid << QVector<Direction>{Direction::North, Direction::East, Direction::Top, Direction::Bottom};
    QTest::newRow("wn edge") << low << high << mid << QVector<Direction>{Direction::South, Direction::East, Direction::Top, Direction::Bottom};
    QTest::newRow("ne edge") << high << high << mid << QVector<Direction>{Direction::South, Direction::West, Direction::Top, Direction::Bottom};
    QTest::newRow("se edge") << high << low << mid << QVector<Direction>{Direction::North, Direction::West, Direction::Top, Direction::Bottom};
    QTest::newRow("wt edge") << low << mid << high << QVector<Direction>{Direction::North, Direction::South, Direction::East, Direction::Bottom};
    QTest::newRow("nt edge") << mid << high << high << QVector<Direction>{Direction::South, Direction::East, Direction::West, Direction::Bottom};
    QTest::newRow("et edge") << high << mid << high << QVector<Direction>{Direction::North, Direction::South, Direction::West, Direction::Bottom};
    QTest::newRow("st edge") << mid << low << high << QVector<Direction>{Direction::North, Direction::East, Direction::West, Direction::Bottom};
    QTest::newRow("n side") << mid << high << mid << QVector<Direction>{Direction::South, Direction::East, Direction::West, Direction::Top, Direction::Bottom};
    QTest::newRow("e side") << high << mid << mid << QVector<Direction>{Direction::North, Direction::South, Direction::West, Direction::Top, Direction::Bottom};
    QTest::newRow("s south") << mid << low << mid << QVector<Direction>{Direction::North, Direction::East, Direction::West, Direction::Top, Direction::Bottom};
    QTest::newRow("w side") << low << mid << mid << QVector<Direction>{Direction::North, Direction::South, Direction::East, Direction::Top, Direction::Bottom};
    QTest::newRow("t side") << mid << mid << high << QVector<Direction>{Direction::North, Direction::South, Direction::East, Direction::West, Direction::Bottom};
    QTest::newRow("b side") << mid << mid << low << QVector<Direction>{Direction::North, Direction::South, Direction::East, Direction::West, Direction::Top};
}

void MazeBuilderTest::TestDirections()
{
    QFETCH(unsigned int, x);
    QFETCH(unsigned int, y);
    QFETCH(unsigned int, z);
    QFETCH(QVector<Direction>, directions);

    Position pos{x, y, z};
    std::unordered_set<Direction> walls;
    for(auto direction : directions) {
        walls.insert(direction);
    }

    MazeBuilder builder(Maze(Size{3, 3, 3}));

    std::unordered_set<Direction> builderDirections;
    for(auto direction : builder.directions(pos)) {
        builderDirections.insert(direction);
    }

    QCOMPARE(builderDirections, walls);
}

void MazeBuilderTest::TestSetWall_data()
{
    QTest::addColumn<Position>("pos");
    QTest::addColumn<Direction>("direction");

    for(int i=0 ; i < 10 ; ++i) {
        unsigned int x = TestChasis::RandInt(0, 4);
        unsigned int y = TestChasis::RandInt(0, 4);
        unsigned int z = TestChasis::RandInt(0, 4);
        QTest::newRow(qPrintable(QString::number(i))) << Position{x, y, z} << DIRECTION_VECTOR.at(TestChasis::RandInt(0, DIRECTION_VECTOR.size()-1));
    }
}

void MazeBuilderTest::TestSetWall()
{
    QFETCH(Position, pos);
    QFETCH(Direction, direction);

    MazeBuilder builder{Maze{Size{5, 5, 5}}};
    builder.setWall(pos, direction, false);

    QCOMPARE(builder.getMaze().isWall(pos, direction), false);
}
