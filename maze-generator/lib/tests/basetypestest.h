#ifndef BASETYPESTEST_H
#define BASETYPESTEST_H

#include <QObject>
#include <QPointer>
#include <QtTest>

namespace Test {

    class BaseTypesTest : public QObject
    {
            Q_OBJECT

        public:
            explicit BaseTypesTest(QObject *parent = 0);

        signals:

        public slots:

        private:

        private Q_SLOTS:

            void initTestCase();
            void cleanupTestCase();
            void init();
            void cleanup();

            void TestPositionEquality_data();
            void TestPositionEquality();
            void TestCalculatePosition_data();
            void TestCalculatePosition();
    };

}

#endif // BASETYPESTEST_H
