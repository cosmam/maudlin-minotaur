#include "wallstest.h"
using namespace Test;

#include "lib/walls.h"

#include "unit_test/testchasis.h"

Q_DECLARE_METATYPE(Direction)
Q_DECLARE_METATYPE(Size)

WallsTest::WallsTest(QObject *parent) :
    QObject(parent)
{
}

void WallsTest::initTestCase()
{
}

void WallsTest::cleanupTestCase()
{
}

void WallsTest::init()
{
}

void WallsTest::cleanup()
{
}

void WallsTest::TestSize_data()
{
    QTest::addColumn<Size>("value");

    for(int i=0 ; i<10 ; ++i) {
        unsigned int width = (unsigned int)TestChasis::RandInt(1, 100);
        unsigned int depth = (unsigned int)TestChasis::RandInt(1, 100);
        unsigned int height = (unsigned int)TestChasis::RandInt(1, 100);
        QTest::newRow(qPrintable(QString::number(i))) << Size{width, depth, height};
    }
}

void WallsTest::TestSize()
{
    QFETCH(Size, value);
    Walls<int> walls(value, 0);

    QCOMPARE(walls.size(), value);
}

void WallsTest::TestData_data()
{
    QTest::addColumn<int>("value");
    QTest::addColumn<Direction>("direction");

    QTest::newRow("north") << 3 << Direction::North;
    QTest::newRow("east") << 3 << Direction::East;
    QTest::newRow("south") << 4 << Direction::South;
    QTest::newRow("west") << 4 << Direction::West;
    QTest::newRow("top") << 5 << Direction::Top;
    QTest::newRow("bottom") << 6 << Direction::Bottom;
}

void WallsTest::TestData()
{
    QFETCH(int, value);
    QFETCH(Direction, direction);

    Walls<int> walls(Size{1, 1, 1}, 0);
    Position pos{0, 0, 0};
    walls.setData(pos, direction, value);

    for(auto dir : AllDirections) {
        if(dir == direction) {
            QCOMPARE(walls.getData(pos, direction), value);
        } else {
            QCOMPARE(walls.getData(pos, dir), 0);
        }
    }
}

void WallsTest::TestComplexIndices_data()
{
    QTest::addColumn<unsigned int>("x_base");
    QTest::addColumn<unsigned int>("y_base");
    QTest::addColumn<unsigned int>("z_base");
    QTest::addColumn<Direction>("direction");
    QTest::addColumn<Direction>("opposite");
    QTest::addColumn<int>("value");
    QTest::addColumn<unsigned int>("x_new");
    QTest::addColumn<unsigned int>("y_new");
    QTest::addColumn<unsigned int>("z_new");

    unsigned int x = 1;
    unsigned int y = 2;
    unsigned int z = 3;

    QTest::newRow("north") << x << y << z << Direction::East << Direction::West << 1 << x+1 << y << z;
    QTest::newRow("south") << x << y << z << Direction::West << Direction::East << 2 << x-1 << y << z;
    QTest::newRow("east") << x << y << z << Direction::North << Direction::South << 3 << x << y+1 << z;
    QTest::newRow("west") << x << y << z << Direction::South << Direction::North << 4 << x << y-1 << z;
    QTest::newRow("top") << x << y << z << Direction::Top << Direction::Bottom << 5 << x << y << z+1;
    QTest::newRow("bottom") << x << y << z << Direction::Bottom << Direction::Top << 6 << x << y << z-1;
}

void WallsTest::TestComplexIndices()
{
    QFETCH(unsigned int, x_base);
    QFETCH(unsigned int, y_base);
    QFETCH(unsigned int, z_base);
    QFETCH(Direction, direction);
    QFETCH(Direction, opposite);
    QFETCH(int, value);
    QFETCH(unsigned int, x_new);
    QFETCH(unsigned int, y_new);
    QFETCH(unsigned int, z_new);

    Walls<int> walls{Size{3, 4, 5}, 0};

    Position base_pos{x_base, y_base, z_base};
    Position new_pos{x_new, y_new, z_new};
    walls.setData(base_pos, direction, value);
    QCOMPARE(walls.getData(new_pos, opposite), value);
}
