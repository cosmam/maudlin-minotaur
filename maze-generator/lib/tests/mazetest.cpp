#include "mazetest.h"
using namespace Test;

#include <unordered_set>

#include <QVector>

#include "lib/maze.h"

#include "unit_test/testchasis.h"

Q_DECLARE_METATYPE(Direction)
Q_DECLARE_METATYPE(Size)

MazeTest::MazeTest(QObject *parent) :
    QObject(parent)
{
}

void MazeTest::initTestCase()
{

}

void MazeTest::cleanupTestCase()
{

}

void MazeTest::init()
{
}

void MazeTest::cleanup()
{
}

void MazeTest::TestSize_data()
{
    QTest::addColumn<Size>("value");

    for(int i=0 ; i<10 ; ++i) {
        unsigned int width = (unsigned int)TestChasis::RandInt(1, 10000);
        unsigned int depth = (unsigned int)TestChasis::RandInt(1, 10000);
        unsigned int height = (unsigned int)TestChasis::RandInt(1, 10000);
        QTest::newRow(qPrintable(QString::number(i))) << Size{width, depth, height};
    }
}

void MazeTest::TestSize()
{
    QFETCH(Size, value);
    Maze maze(value);

    QCOMPARE(maze.size(), value);
}

void MazeTest::TestWall_data()
{
    QTest::addColumn<Direction>("direction");

    QTest::newRow("north") << Direction::North;
    QTest::newRow("east") << Direction::East;
    QTest::newRow("south") << Direction::South;
    QTest::newRow("west") << Direction::West;
    QTest::newRow("top") << Direction::Top;
    QTest::newRow("bottom") << Direction::Bottom;
}

void MazeTest::TestWall()
{
    QFETCH(Direction, direction);

    Maze maze(Size{1, 1, 1});
    Position pos{0, 0, 0};
    maze.setWall(pos, direction, true);

    QCOMPARE(maze.isWall(pos, direction), true);
}

void MazeTest::TestWalls_data()
{
    QTest::addColumn<QVector<Direction>>("directions");

    QTest::newRow("1") << QVector<Direction>{Direction::North, Direction::Top};
    QTest::newRow("2") << QVector<Direction>{Direction::North, Direction::Top, Direction::South, Direction::East};
    QTest::newRow("3") << QVector<Direction>();
    QTest::newRow("4") << QVector<Direction>{Direction::North, Direction::Top, Direction::South, Direction::East, Direction::West, Direction::Bottom};
}

void MazeTest::TestWalls()
{
    QFETCH(QVector<Direction>, directions);

    Maze maze(Size{1, 1, 1});
    Position pos{0, 0, 0};
    maze.setWall(pos, Direction::North, false);
    maze.setWall(pos, Direction::East, false);
    maze.setWall(pos, Direction::South, false);
    maze.setWall(pos, Direction::West, false);
    maze.setWall(pos, Direction::Top, false);
    maze.setWall(pos, Direction::Bottom, false);

    std::unordered_set<Direction> direction_set;
    for(auto direction : directions) {
        maze.setWall(pos, direction, true);
        direction_set.insert(direction);
    }

    QCOMPARE(maze.walls(pos), direction_set);
}
