#ifndef SOLUTIONSTEPPER_H
#define SOLUTIONSTEPPER_H

#include "basetypes.h"
#include "mazebuilder.h"
#include "optional.h"
#include "solver.h"

#include <map>

enum class SteppingState {
    Initial,
    Start_Of_Find_Path,
    Stepping_Find,
    End_Of_Find_Path,
    Stepping_Visit,
    End_Of_Visit_Path,
    Finished
};

class SolutionStepper
{
public:
    SolutionStepper();

    SteppingState getState() const { return _state; }

    std::vector<MazeRoom> setSolution(Solver::Solution solution);

    std::vector<MazeRoom> reset();

    std::vector<MazeRoom> step();

    std::vector<MazeRoom> finish();

private:

    std::vector<MazeRoom> step_initial();

    std::vector<MazeRoom> start_find_path();

    std::vector<MazeRoom> step_find_single();

    std::vector<MazeRoom> step_visit_single();

    std::vector<MazeRoom> step_find_path();

    std::vector<MazeRoom> step_visit_path();

    std::vector<Position> getPositions() const;

    std::vector<MazeRoom> getRooms() const;

    tl::optional<Solver::Solution> _solution;
    tl::optional<MazeBuilder> _builder;
    std::vector<Step> _steps;
    std::map<Position, MazeRoom> _rooms;
    unsigned int _path_index;
    unsigned int _step_index;
    SteppingState _state;
};

#endif // SOLUTIONSTEPPER_H
