#include "solver.h"

#include <algorithm>
#include <chrono>
#include <map>
#include <random>

using namespace Solver;

namespace {

std::default_random_engine rng(std::chrono::steady_clock::now().time_since_epoch().count());

int rand_int(int value) {
    std::uniform_int_distribution<int> distribution(0,value-1);
    return distribution(rng);
}

std::vector<Direction> direction_shuffle(std::vector<Direction> direction) {
    std::random_shuffle(direction.begin(), direction.end(), rand_int);
    return direction;
}

unsigned int random_uint(unsigned int min, unsigned int max) {
    std::uniform_int_distribution<unsigned int> distribution(min,max);
    return distribution(rng);
}

}

Path Solver::step(const MazeBuilder & builder, const Constraints & constraints, const Path & path, direction_shuffle_func func) {
    // this assumes there is always an unconstrained direction; choice of unvisited locations must account for this
    auto directions = std::vector<Direction>(getDirections(builder.getMaze().size(), path.current));
    auto it = directions.begin();
    while(it != directions.end()) {
        if(constraints.getConstraint(path.current, *it) == WallConstraint::Exists) {
            it = directions.erase(it);
        } else {
            ++it;
        }
    }
    directions = func(directions);

    Path newPath = path;
    newPath.steps.push_back(Step{path.current, directions.front()});
    newPath.current = calculatePosition(path.current, directions.front());
    newPath.isFinished = builder.isVisited(newPath.current);

    return newPath;
}

Path Solver::findPath(const MazeBuilder & builder, const Constraints & constraints, Position start, direction_shuffle_func func) {
    auto path = Path{start, std::vector<Step>(), false};
    while(!path.isFinished) {
        path = step(builder, constraints, path, func);
    }
    return path;
}

std::vector<Step> Solver::getSteps(const Path & path) {
    std::vector<Step> steps;
    std::map<Position, Direction> directions;
    for(auto step : path.steps) {
        directions[step.position] = step.direction;
    }

    Position next(path.steps.front().position);
    while(directions.count(next) > 0) {
        auto step = Step{next, directions[next]};
        steps.push_back(step);
        next = calculatePosition(step.position, step.direction);
    }

    return steps;
}

MazeBuilder Solver::stepPath(MazeBuilder builder, Step step) {
    auto walls = builder.getMaze().walls(step.position);
    walls.erase(step.direction);
    builder.visit(step.position, walls);
    return builder;
}


MazeBuilder Solver::walkPath(MazeBuilder builder, const Path & path) {
    for(auto step : getSteps(path)) {
        builder = stepPath(builder, step);
    }

    return builder;
}

Path Solver::findConstrainedPath(MazeBuilder builder, const Constraints & constraints, std::pair<Position, Position> & connection, direction_shuffle_func dir_func) {
    builder.visit(connection.second, AllDirections);
    return findPath(builder, constraints, connection.first, dir_func);
}

Solution Solver::solveRequiredConnections(MazeBuilder builder, const Constraints & constraints, direction_shuffle_func dir_func, random_uint_func uint_func) {
    std::vector<Path> paths;
    for(auto connection : constraints.getConnections()) {
        paths.push_back(findConstrainedPath(builder, constraints, connection, dir_func));
    }

    for(const Path & path : paths) {
        builder = walkPath(builder, path);
    }

    if(paths.empty()) {
        builder.visit(builder.randomUnivisited(uint_func), AllDirections);
    }

    return Solution{builder, constraints, paths};
}

MazeBuilder Solver::eraseWalls(MazeBuilder builder, const Constraints & constraints) {
    for(auto position : generatePositions(constraints.size())) {
        for(auto direction : AllDirections) {
            if(constraints.getConstraint(position, direction) == WallConstraint::Removed) {
                builder.setWall(position, direction, false);
            }
        }
    }
    return builder;
}

Solution Solver::solve(const Maze & maze, const Constraints & constraints, direction_shuffle_func dir_func, random_uint_func uint_func) {
    auto solution = solveRequiredConnections(MazeBuilder{maze}, constraints, dir_func, uint_func);
    solution.builder = eraseWalls(solution.builder, constraints);

    while(!solution.builder.isFullyVisited()) {
        auto start = solution.builder.randomUnivisited(uint_func);
        solution.paths.push_back(findPath(solution.builder, solution.constraints, start, dir_func));
        solution.builder = walkPath(solution.builder, solution.paths.back());
    }

    return solution;
}

Solution Solver::solve(const Maze &maze, const Constraints &constraints) {
    return solve(maze, constraints, direction_shuffle, random_uint);
}
