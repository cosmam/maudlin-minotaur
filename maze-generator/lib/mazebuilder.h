#ifndef MAZEBUILDER_H
#define MAZEBUILDER_H

#include <random>
#include <vector>

#include "basetypes.h"
#include "maze.h"

class MazeBuilder {
public:
    MazeBuilder(Maze maze);

    Maze getMaze() const { return _maze; };

    Position randomUnivisited(random_uint_func func) const;

    void setWall(Position position, Direction wall, bool exists);

    void visit(Position position, const std::unordered_set<Direction> & walls);

    bool isVisited(Position position) const;

    bool isFullyVisited() const;

private:

    MazeBuilder(Maze maze, const std::vector<bool> & visited);

    unsigned int getIndex(Position position) const;

    Position getPosition(unsigned int index) const;

    Maze _maze;
    std::vector<bool> _visited;
};

#endif // MAZEBUILDER_H
