#ifndef MAZE_H
#define MAZE_H

#include <unordered_set>
#include <vector>

#include "basetypes.h"
#include "walls.h"

class Maze {
public:
    Maze(Size size);

    Size size() const { return _walls.size(); }

    bool isWall(Position position, Direction wall) const;

    std::unordered_set<Direction> walls(Position position) const;

    void setWall(Position position, Direction wall, bool exists);

private:

    Walls<bool> _walls;
};

#endif // MAZE_H
