#include "constraints.h"

Constraints::Constraints(Size size)
    : _walls(size, WallConstraint::Unconstrained)
{};

WallConstraint Constraints::getConstraint(Position position, Direction wall) const {
    return _walls.getData(position, wall);
}

void Constraints::setConstraint(Position position, Direction wall, WallConstraint constraint) {
    _walls.setData(position, wall, constraint);
}

std::vector<std::pair<Position, Position>> Constraints::getConnections() const {
    return _connections;
}

void Constraints::setConnections(std::vector<std::pair<Position, Position>> connections) {
    _connections = connections;
}
