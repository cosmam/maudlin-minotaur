#ifndef BASE_TYPES_H
#define BASE_TYPES_H

#include<unordered_set>
#include<set>
#include<vector>

enum class Direction { North, East, South, West, Top, Bottom };
const std::unordered_set<Direction> AllDirections{Direction::North, Direction::East, Direction::South, Direction::West, Direction::Top, Direction::Bottom};

enum class WallConstraint {Unconstrained, Removed, Exists};

enum class RoomState {
    Visited,
    Unvisited,
    OnPath,
    Current
};

enum class MazeType {
    Two_Dimensional,
    Three_Dimensional
};

typedef std::vector<Direction> (*direction_shuffle_func)(std::vector<Direction>);

typedef unsigned int (*random_uint_func)(unsigned int, unsigned int);

struct Position {
    unsigned int x;
    unsigned int y;
    unsigned int z;
};

struct Size {
    unsigned int width;
    unsigned int depth;
    unsigned int height;
};

struct Step {
    Position position;
    Direction direction;
};

struct Path {
    Position current;
    std::vector<Step> steps;
    bool isFinished;
};

struct MazeRoom {
    Position position;
    std::unordered_set<Direction> walls;
    RoomState state;
};

bool operator==(const Position & lhs, const Position & rhs);

bool operator!=(const Position & lhs, const Position & rhs);

bool operator<(const Position & lhs, const Position & rhs);

bool operator==(const Size & lhs, const Size & rhs);

Position calculatePosition(Position position, Direction direction);

std::vector<std::pair<Position,Position>> generatePairs(const std::set<Position> & positions);

std::vector<Position> generatePositions(Size size);

std::vector<Direction> getDirections(Size size, Position pos);

#endif // BASE_TYPES_H
