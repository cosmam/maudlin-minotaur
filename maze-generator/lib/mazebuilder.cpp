#include "mazebuilder.h"

#include <algorithm>
#include <assert.h>

MazeBuilder::MazeBuilder(Maze maze)
    : MazeBuilder(maze, std::vector<bool>(maze.size().width * maze.size().depth * maze.size().height, false))
{}

MazeBuilder::MazeBuilder(Maze maze, const std::vector<bool> & visited)
    : _maze(maze),
      _visited(visited)
{}

Position MazeBuilder::randomUnivisited(random_uint_func func) const {
    assert(!this->isFullyVisited());

    unsigned int index;
    do {
        index = func(0, _visited.size()-1);
    } while(_visited.at(index));

    return this->getPosition(index);
}

void MazeBuilder::setWall(Position position, Direction wall, bool exists)
{
    _maze.setWall(position,wall, exists);
}

void MazeBuilder::visit(Position position, const std::unordered_set<Direction> & walls) {
    _visited.at(this->getIndex(position)) = true;
    for(auto direction : AllDirections) {
        _maze.setWall(position, direction, walls.count(direction) > 0);
    }
}

bool MazeBuilder::isVisited(Position position) const {
    return _visited.at(this->getIndex(position));
}

bool MazeBuilder::isFullyVisited() const {
    return std::all_of(_visited.begin(), _visited.end(), [=](bool visited) { return visited; });
}

Position MazeBuilder::getPosition(unsigned int index) const {
    assert(index < _visited.size());

    Position position;
    position.x = index;
    position.z = position.x / (_maze.size().width * _maze.size().depth);
    position.x -= (position.z * _maze.size().width * _maze.size().depth);
    position.y = position.x / _maze.size().width;
    position.x -= (position.y * _maze.size().width);

    return position;
}

unsigned int MazeBuilder::getIndex(Position position) const {
    assert(position.x < (_maze.size().width));
    assert(position.y < (_maze.size().depth));
    assert(position.z < (_maze.size().height));

    return _maze.size().depth * _maze.size().width * position.z + _maze.size().width * position.y + position.x;
}
