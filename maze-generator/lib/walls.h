#ifndef WALLS_H
#define WALLS_H

#include <assert.h>
#include <vector>

#include "basetypes.h"

template <class T>
class Walls {
public:

    Walls(Size size, T defaultValue)
        : _size(size),
          _x_offset(_size.width*(_size.depth+1)*_size.height),
          _y_offset(_size.depth*(_size.width+1)*_size.height),
          _walls(std::vector<T>((_x_offset+_y_offset+(_size.width*_size.depth*(_size.height+1))), defaultValue))
    {}

    Size size() const { return _size; }

    T getData(Position position, Direction wall) const {
        return _walls.at(this->getIndex(position, wall));
    }

    void setData(Position position, Direction wall, T data) {
        _walls.at(this->getIndex(position, wall)) = data;
    }

private:

    unsigned int getIndex(Position position, Direction wall) const {
        assert(position.x < _size.width);
        assert(position.y < _size.depth);
        assert(position.z < _size.height);

        unsigned int i = (wall == Direction::East ? position.x + 1 : position.x);
        unsigned int j = (wall == Direction::North ? position.y + 1 : position.y);
        unsigned int k = (wall == Direction::Top ? position.z + 1 : position.z);

        unsigned int index;
        if ((wall == Direction::South || wall == Direction::North)) {
            index = (_size.depth + 1) * _size.width * k + _size.width * j + i;
        } else if((wall == Direction::East || wall == Direction::West)) {
            index = _x_offset + (_size.width + 1) * _size.depth * k + (_size.width + 1) * j + i;
        } else {
            index = _x_offset + _y_offset + _size.width * _size.depth * k + _size.width * j + i;
        }

        return index;
    }

    Size _size;
    unsigned int _x_offset;
    unsigned int _y_offset;
    std::vector<T> _walls;
};

#endif // WALLS_H
