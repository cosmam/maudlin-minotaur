QT += gui core testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG += c++11 console
CONFIG -= app_bundle

QMAKE_CXXFLAGS += --coverage
QMAKE_LDFLAGS += --coverage
LIBS += -lgcov

CFLAGS += --coverage
LDFLAGS += --coverage

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        lib/tests/basetypestest.cpp \
        lib/tests/mazebuildertest.cpp \
        lib/tests/mazetest.cpp \
        lib/tests/solvertest.cpp \
        lib/tests/wallstest.cpp \
        unit_test/main.cpp \
        unit_test/testchasis.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

include(maze-generator.pri)

HEADERS += \
    lib/tests/basetypestest.h \
    lib/tests/mazebuildertest.h \
    lib/tests/mazetest.h \
    lib/tests/solvertest.h \
    lib/tests/wallstest.h \
    unit_test/testchasis.h
