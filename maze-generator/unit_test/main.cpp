#include <QCoreApplication>

#include <memory>

#include "testchasis.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    auto chasis = std::unique_ptr<Test::TestChasis>(new Test::TestChasis());

    chasis->CreateObjects();
    return chasis->RunTests();
}
