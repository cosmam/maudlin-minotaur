#include "testchasis.h"
#include "lib/tests/basetypestest.h"
#include "lib/tests/mazebuildertest.h"
#include "lib/tests/mazetest.h"
#include "lib/tests/solvertest.h"
#include "lib/tests/wallstest.h"

#include <QtGlobal>
#include <QDebug>
#include <QRandomGenerator>
#include <QTest>
#include <QTime>

using namespace Test;

namespace {
    QRandomGenerator rnd = QRandomGenerator::securelySeeded();
}

TestChasis::TestChasis(QObject *parent) :
    QObject(parent)
{
}

TestChasis::~TestChasis()
{
    qDeleteAll(_objects);
}

void TestChasis::CreateObjects()
{
    RegisterTest(new BaseTypesTest(this));
    RegisterTest(new MazeBuilderTest(this));
    RegisterTest(new MazeTest(this));
    RegisterTest(new SolverTest(this));
    RegisterTest(new WallsTest(this));
}

int TestChasis::RunTests()
{
    int testStatus;

    _status = 0;
    _errors.clear();

    foreach(QObject * object, _objects ) {
        testStatus = QTest::qExec(object);

        if( testStatus != 0 ) {
            _errors.append( QString( object->metaObject()->className() ) );        // LCOV_EXCL_LINE
        }

        _status |= testStatus;
    }

    if( !_errors.isEmpty() ) {
        qWarning() << "********** Testing Results!**********\n\nErrors in the following classes:" << _errors       // LCOV_EXCL_LINE
                   << "\n\n*************************************";                                                  // LCOV_EXCL_LINE
    } else {
        qWarning() << "********** Testing Results!**********\n\nAll tests passed!"
                   << "\n\n*************************************";
    }

    return _status;
}

int TestChasis::RandInt(int lower, int upper)
{
    return (rnd() % ((upper + 1) - lower) + lower);
}

QString TestChasis::RandString()
{
    QString retString("");
    int length = RandInt(1,1000);

    for( int i=0 ; i < length ; ++i ) {
        retString += QChar((ushort)RandInt(10,255));
    }

    return retString;
}

QDate TestChasis::RandDate(QDate start, QDate end)
{
    QDate date(1,1,1);

    while( !( (start <= date) && (date <= end) ) ) {

        int year = RandInt(start.year(),end.year());
        int month = RandInt(1,12);
        int day = RandInt(1, QDate(year, month, 1).daysInMonth());
        date.setDate(year, month, day);
    }

    return date;
}

QColor TestChasis::RandColor()
{
    int red = RandInt(0,255);
    int blue = RandInt(0,255);
    int green = RandInt(0,255);

    return QColor(red, blue, green);
}

void TestChasis::RegisterTest(QObject *object)
{
    if( !_objects.contains(object) ) {
        _objects.append(object);
    }
}
